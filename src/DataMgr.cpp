#include "framework/CException.h"
#include "framework/Config.h"
#include "framework/Utils.h"
#include "http/identification/Identifier.h"
#include "DataMgr.h"

#include <stdlib.h>

DataMgr* DataMgr::_instance = NULL;

void DataMgr::Initialize()
{
	_instance = new DataMgr();
}

void DataMgr::Deinitialize()
{
	if (_instance)
		delete _instance;

	_instance = NULL;
}

DataMgr::DataMgr() : _endCert(NULL), _endCertPK(NULL)
{
	LOG_TRACE("DataMgr::DataMgr");

	LoadData();
}

DataMgr::~DataMgr()
{
	LOG_TRACE("DataMgr::~DataMgr");

	ReleaseData();
}

void DataMgr::LoadData()
{
	LOG_TRACE("DataMgr::LoadData");

	if (CONFIG->GetBoolValue(PROXY_HTTPS_ENABLE))
	{
		_endCert = new Certificate();
		_endCert->ImportPEM(CONFIG->GetStrValue(CERT_END_CERT_PATH).c_str());

		_endCertPK = new PrivateKey();
		_endCertPK->ImportPEM(CONFIG->GetStrValue(CERT_END_CERT_PK_PATH).c_str(), CONFIG->GetStrValue(CERT_END_CERT_PK_PASS).c_str());

		std::string extraCertPaths = CONFIG->GetStrValue(CERT_EXTRA_CERTS_PATH);
		size_t extraCertsCount = std::count(extraCertPaths.begin(), extraCertPaths.end(), ';');
		if (!extraCertPaths.empty())
			extraCertsCount++;

		LOG_DEBUGF("DataMgr::LoadData - Expecting %lu extra certs ...", extraCertsCount);

		if (extraCertsCount > 0)
		{
			_extraCerts.reserve(extraCertsCount);

			size_t nextPos = 0;
			size_t actualPos = 0;
			do
			{
				nextPos = extraCertPaths.find(";", actualPos);
				std::string extraCertPath = extraCertPaths.substr(actualPos, nextPos - actualPos);
				actualPos = nextPos + 1;

				Certificate* extraCert = new Certificate();
				extraCert->ImportPEM(extraCertPath.c_str());

				_extraCerts.push_back(extraCert);

			} while (nextPos != std::string::npos);
		}

		std::string trustCertPaths = CONFIG->GetStrValue(CERT_TRUST_CERTS_PATH);
		size_t trustCertsCount = std::count(trustCertPaths.begin(), trustCertPaths.end(), ';');
		if (!trustCertPaths.empty())
			trustCertsCount++;

		LOG_DEBUGF("DataMgr::LoadData - Expecting %lu trusted certs ...", trustCertsCount);

		if (trustCertsCount > 0)
		{
			_trustCerts.reserve(trustCertsCount);

			size_t nextPos = 0;
			size_t actualPos = 0;
			do
			{
				nextPos = trustCertPaths.find(";", actualPos);
				std::string extraCertPath = trustCertPaths.substr(actualPos, nextPos - actualPos);
				actualPos = nextPos + 1;

				Certificate* trustCert = new Certificate();
				trustCert->ImportPEM(extraCertPath.c_str());

				_trustCerts.push_back(trustCert);

			} while (nextPos != std::string::npos);
		}
	}

	if (Identifier::IsRequiredSecretKey((HttpHeaderIdentifiers)CONFIG->GetIntValue(HTTP_IDENTIFICATOR)))
	{
		if (CONFIG->GetStrValue(HTTP_IDENTIFY_SECRET_KEY_PATH).c_str() == NULL)
		{
			std::string msg = "DataMgr::LoadData - Required http identificator key is not set!";
			if (CONFIG->GetBoolValue(HTTP_IDENTIFY_REQUIRED))
			{
				LOG_ERROR(msg.c_str());
				CException::Throw(CException::InputDataError, "Required http identificator key is not set!");
			}
			else
				LOG_WARNING(msg.c_str());
		}
		else
		{
			FILE* keyFile = fopen(CONFIG->GetStrValue(HTTP_IDENTIFY_SECRET_KEY_PATH).c_str(), "r");
			if (keyFile == NULL || ferror(keyFile) != 0)
			{
				std::string msg = "DataMgr::LoadData - Required http identificator key cannot be opened!";
				if (CONFIG->GetBoolValue(HTTP_IDENTIFY_REQUIRED))
				{
					LOG_ERROR(msg.c_str());
					CException::Throw(CException::InputDataError, "Required http identificator key cannot be opened!");
				}
				else
					LOG_WARNING(msg.c_str());
			}
			else
			{
				char line[256];

				if (fgets(line, sizeof(line), keyFile))
				{
					_httpIdentificatorSecretKey = line;
					_httpIdentificatorSecretKey.erase(std::remove(_httpIdentificatorSecretKey.begin(), _httpIdentificatorSecretKey.end(), '\n'), _httpIdentificatorSecretKey.end());
				}
			}

			if (keyFile)
				fclose(keyFile);
		}
	}
}

void DataMgr::ReleaseData()
{
	LOG_TRACE("DataMgr::ReleaseData");

	if (_endCert)
	{
		delete _endCert;
		_endCert = NULL;
	}

	if (_endCertPK)
	{
		delete _endCertPK;
		_endCertPK = NULL;
	}

	for (CertificatePtrStore::iterator itr = _extraCerts.begin(); itr != _extraCerts.end(); itr++)
	{
		if (*itr)
			delete *itr;
	}
	
	for (CertificatePtrStore::iterator itr = _trustCerts.begin(); itr != _trustCerts.end(); itr++)
	{
		if (*itr)
			delete *itr;
	}
}
