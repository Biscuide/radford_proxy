#pragma once

#include "Radford.h"

class Session;
class SocketConnector;
struct SessionInfo;
class SessionWorkerPool;

class Proxy
{

public:

	Proxy();
	virtual ~Proxy();

    void Start(const SessionInfo* pSessionInfo);

    bool IsClosing() const { return _bClosing == true; }
	void SetClose() { _bClosing = true; }

protected:

	virtual void Open() = 0;
	virtual void Close() = 0;
	virtual void Accept(SessionWorkerPool& pool) = 0;
    void Listen();

protected:

    int _iSocketFD;
	const SessionInfo* _info;
	SocketConnector* _pSocketConn;

    bool _bClosing;
};
