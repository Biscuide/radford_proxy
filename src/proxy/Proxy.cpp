#include "proxy/Proxy.h"
#include "session/Session.h"
#include "session/SessionWorkerPool.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread>

#ifdef _WIN32

/*
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <windows.h>
*/


#else

/*
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
*/
#include <signal.h>

#endif

Proxy::Proxy() : _iSocketFD(SOCKET_UNDEF), _info(NULL), _bClosing(false)
{
	LOG_TRACE("Proxy::Proxy");

	_pSocketConn = SocketConnector::Create();
}

Proxy::~Proxy()
{
	LOG_TRACE("Proxy::~Proxy");

	SocketConnector::Destroy(_pSocketConn);
}

void Proxy::Start(const SessionInfo* pSessionInfo)
{
	LOG_TRACE("Proxy::Start");

	_info = pSessionInfo;

	LOG_DEBUGF("Proxy::Start - Port: %u", CONFIG->GetIntValue(_info->configPort[CONNECTION_CLIENT]));

	try
	{
		Open();

		std::thread thr(&Proxy::Listen, this);

		thr.join();
	}
	catch (const CException& ex)
	{
		PrintEx(ex);
	}

	try
	{
		Close();
	}
	catch (const CException& ex)
	{
		PrintEx(ex);
	}
}

#ifndef _WIN32
/* Catch Signal Handler function */
void signal_callback_handler(int signum)
{
	LOG_ERRORF("signal_callback_handler: %i", signum);
}
#endif

void Proxy::Listen()
{
	LOG_TRACE("Proxy::Listen");

    SessionWorkerPool pool;

    try
    {
        pool.StartWorkers();

#ifndef _WIN32
		signal(SIGPIPE, signal_callback_handler);
#endif

        struct timeval timeout;
        timeout.tv_sec = CONNECTION_CHECK_PERIOD;
        timeout.tv_usec = 0;
        fd_set sockets;

        FD_ZERO(&sockets);
        FD_SET(_iSocketFD, &sockets);

        while (!IsClosing())
        {
            //LOG_DEBUGT(_iSocketFD, "Proxy::Listen - Waiting for client ...")

            if (select(_iSocketFD + 1, &sockets, NULL, NULL, &timeout) < 0)
            {
				LOG_ERRORT(_iSocketFD, "Proxy::Listen - Error in select");
                break;
            }

            if (!FD_ISSET(_iSocketFD, &sockets))
            {
                //LOG_DEBUGT(_iSocketFD, "Proxy::Listen - Checking ...")

                timeout.tv_sec = CONNECTION_CHECK_PERIOD;
                timeout.tv_usec = 0;

                FD_ZERO(&sockets);
                FD_SET(_iSocketFD, &sockets);
                continue;
            }

			Accept(pool);
        }
    }
    catch (const CException& ex)
    {
		PrintEx(ex);
    }

    pool.StopWorkers();
}
