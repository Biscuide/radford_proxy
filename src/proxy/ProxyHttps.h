#pragma once

#include "proxy/ProxyHttp.h"
#include "Radford.h"

#include <openssl/ssl.h>

class SessionWorkerPool;

class ProxyHttps : public ProxyHttp
{

public:

	ProxyHttps();
	virtual ~ProxyHttps();

protected:

	virtual void Open();
	virtual void Close();
	virtual void Accept(SessionWorkerPool& pool);

private:

	SSL_CTX* _pSSLCtx;

};