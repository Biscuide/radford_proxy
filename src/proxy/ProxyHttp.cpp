#include "proxy/ProxyHttp.h"
#include "socket/SocketConnector.h"
#include "session/SessionWorkerPool.h"
#include "session/Session.h"

ProxyHttp::ProxyHttp() : Proxy()
{
	LOG_TRACE("ProxyHttp::ProxyHttp");
}

ProxyHttp::~ProxyHttp()
{
	LOG_TRACE("ProxyHttp::~ProxyHttp");

	if (_iSocketFD != SOCKET_UNDEF)
		Close();
}

void ProxyHttp::Open()
{
	LOG_TRACE("ProxyHttp::Open");

	if (_pSocketConn == NULL)
	{
		LOG_ERROR("Proxy::Open - _pSocketConn == NULL");
		CException::Throw(CException::InternalError, "_pSocketConn == NULL");
	}

	_iSocketFD = _pSocketConn->Bind(CONFIG->GetIntValue(_info->configPort[CONNECTION_CLIENT]));

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERROR("Proxy::Open - Error while creating socket");
		CException::Throw(CException::SocketError, "Proxy::Open - Error while creating socket");
	}

	_pSocketConn->Listen(_iSocketFD);
}

void ProxyHttp::Close()
{
	LOG_TRACE("ProxyHttp::Close");

	if (_pSocketConn == NULL)
	{
		LOG_ERROR("Proxy::Close - _pSocketConn == NULL");
		CException::Throw(CException::InternalError, "_pSocketConn == NULL");
	}

	_pSocketConn->Close(_iSocketFD);
}


void ProxyHttp::Accept(SessionWorkerPool& pool)
{
	LOG_TRACE("ProxyHttp::Accept");

	int iClientSocketFD = _pSocketConn->Accept(_iSocketFD);

	if (iClientSocketFD <= 0)
	{
		LOG_ERROR("ProxyHttp::Accept - Error while accepting client");
		return;
	}

	LOG_DEBUGF("ProxyHttp::Accept - Client connected [Socket: %i]", iClientSocketFD);

	pool.EnqueueSession(new Session(iClientSocketFD, _info));
}
