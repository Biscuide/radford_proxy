#pragma once

#include "proxy/Proxy.h"
#include "Radford.h"

class SessionWorkerPool;

class ProxyHttp : public Proxy
{

public:

	ProxyHttp();
	virtual ~ProxyHttp();

protected:

	virtual void Open();
	virtual void Close();
	virtual void Accept(SessionWorkerPool& pool);

private:

};