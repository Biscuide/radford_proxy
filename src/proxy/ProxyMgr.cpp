#include "proxy/ProxyMgr.h"
#include "proxy/ProxyHttp.h"
#include "proxy/ProxyHttps.h"
#include "session/Session.h"

#include <thread>

ProxyMgr* ProxyMgr::_instance = NULL;

const SessionInfo ProxyMgr::_proxyInfo[PROTOCOL_MAX] =
{
	{ PROTOCOL_HTTP, PROXY_HTTP_PORT_INPUT, PROXY_HTTP_PORT_OUTPUT, PROXY_HTTP_ENABLE },
	{ PROTOCOL_HTTPS, PROXY_HTTPS_PORT_INPUT, PROXY_HTTPS_PORT_OUTPUT, PROXY_HTTPS_ENABLE }
};

void ProxyMgr::Initialize()
{
	_instance = new ProxyMgr();
}

void ProxyMgr::Deinitialize()
{
	if (_instance)
		delete _instance;

	_instance = NULL;
}

ProxyMgr::ProxyMgr() : _bClosing(false)
{
	LOG_TRACE("ProxyMgr::ProxyMgr");

	for (uint16 i = 0; i < PROTOCOL_MAX; i++)
	{
		_aProxies[i] = NULL;
	}
}

ProxyMgr::~ProxyMgr()
{
	LOG_TRACE("ProxyMgr::~ProxyMgr");

	if (IsClosing() == false)
		Close();
}

void ProxyMgr::Start()
{
	LOG_TRACE("ProxyMgr::Start");

	for (uint16 i = 0; i < PROTOCOL_MAX; i++)
	{
		if (CONFIG->GetBoolValue(_proxyInfo[i].configEnable) == true)
		{
			_aProxies[i] = CreateProxy(_proxyInfo[i].protocolName);
			_aProxyThreads[i] = std::thread(&Proxy::Start, _aProxies[i], &_proxyInfo[i]);
		}
	}	
}

void ProxyMgr::Close()
{
	LOG_TRACE("ProxyMgr::Close");

	_bClosing = true;

	for (uint16 i = 0; i < PROTOCOL_MAX; i++)
	{
		if (_aProxies[i] != NULL)
			_aProxies[i]->SetClose();
	}

	for (uint16 i = 0; i < PROTOCOL_MAX; i++)
	{
		if (_aProxies[i] != NULL)
		{
			_aProxyThreads[i].join();
			delete _aProxies[i];
		}
	}
}

Proxy * ProxyMgr::CreateProxy(const ProtocolName& protocol)
{
	LOG_TRACE("ProxyMgr::CreateProxy");

	switch (protocol)
	{
		case PROTOCOL_HTTP:
			return new ProxyHttp();
		case PROTOCOL_HTTPS:
			return new ProxyHttps();
		default:
			return NULL;
	}

	return NULL;
}
