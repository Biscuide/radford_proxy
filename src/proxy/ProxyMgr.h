#pragma once

#include "Radford.h"
#include "http/HttpClient.h"

#include <thread>

class Proxy;

class ProxyMgr
{

public:

	inline static ProxyMgr* Instance() { return _instance; }

	static void Initialize();
	static void Deinitialize();

	void Start();

	bool IsClosing() const { return _bClosing == true; }
	void Close();

private:

	ProxyMgr();
	~ProxyMgr();

	Proxy* CreateProxy(const ProtocolName& protocol);

	static ProxyMgr* _instance;
	static const SessionInfo _proxyInfo[PROTOCOL_MAX];

	Proxy* _aProxies[PROTOCOL_MAX];
	std::thread _aProxyThreads[PROTOCOL_MAX];

	bool _bClosing;
};

#define PROXYMGR ProxyMgr::Instance()
