#include "proxy/ProxyHttps.h"
#include "proxy/ProxyHttp.h"
#include "socket/SocketConnector.h"
#include "session/SessionWorkerPool.h"
#include "session/Session.h"
#include "DataMgr.h"

#include <openssl/err.h>

ProxyHttps::ProxyHttps() : ProxyHttp(), _pSSLCtx(NULL)
{
	LOG_TRACE("ProxyHttps::ProxyHttps");
}

ProxyHttps::~ProxyHttps()
{
	LOG_TRACE("ProxyHttps::~ProxyHttps");
}

void ProxyHttps::Open()
{
	LOG_TRACE("ProxyHttps::Open");

	ProxyHttp::Open();

	_pSSLCtx = SSL_CTX_new(TLSv1_2_server_method());
	if (_pSSLCtx == NULL)
	{
		LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Cannot create SSL context (SSL_CTX_new)");
		CException::Throw(CException::OpenSSLError, "Cannot create SSL context (SSL_CTX_new)");
	}

	SSL_CTX_set_options(_pSSLCtx, SSL_OP_SINGLE_DH_USE);

	// Nacteni koncoveho certifikatu
	if (DATAMGR->GetEndCert() == NULL || DATAMGR->GetEndCert()->GetX509() == NULL)
	{
		if (_pSSLCtx != NULL)
			SSL_CTX_free(_pSSLCtx);

		LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - End cert is not set");
		CException::Throw(CException::InternalError, "End cert is not set");
	}

	X509* pEndCert = DATAMGR->GetEndCert()->GetX509();
	if (SSL_CTX_use_certificate(_pSSLCtx, pEndCert) != 1)
	{
		LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Cannot load end certificate (SSL_CTX_use_certificate)");
		CException::Throw(CException::OpenSSLError, "Cannot load end certificate (SSL_CTX_use_certificate)");
	}

	// Nacteni privatniho klice koncoveho certifikatu
	if (DATAMGR->GetEndCertPK() == NULL || DATAMGR->GetEndCertPK()->GetKeyEVP() == NULL)
	{
		if (_pSSLCtx != NULL)
			SSL_CTX_free(_pSSLCtx);

		LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Private key is not set");
		CException::Throw(CException::InternalError, "Private key is not set");
	}

	EVP_PKEY* pPrivateKey = DATAMGR->GetEndCertPK()->GetKeyEVP();
	if (SSL_CTX_use_PrivateKey(_pSSLCtx, pPrivateKey) != 1)
	{
		LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Cannot use private key (SSL_CTX_use_PrivateKey)");
		CException::Throw(CException::OpenSSLError, "Cannot use private key (SSL_CTX_use_PrivateKey)");
	}

	ERR_remove_state(0);

	if (!SSL_CTX_check_private_key(_pSSLCtx))
	{
		LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Private key is not correct (SSL_CTX_check_private_key)");
		CException::Throw(CException::OpenSSLError, "Private key is not correct (SSL_CTX_check_private_key)");
	}

	// Nacteni mezilehlych certifikatu
	const CertificatePtrStore& extraCerts = DATAMGR->GetExtraCerts();
	for (CertificatePtrStore::const_iterator itr = extraCerts.begin(); itr != extraCerts.end(); itr++)
	{
		if (*itr == NULL || (*itr)->GetX509() == NULL)
		{
			if (_pSSLCtx != NULL)
				SSL_CTX_free(_pSSLCtx);

			LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Required extra cert is not set");
			CException::Throw(CException::InternalError, "Required extra cert is not set");
		}

		X509* pExtraCert = (*itr)->GetX509();
		if (SSL_CTX_add_extra_chain_cert(_pSSLCtx, X509_dup(pExtraCert)) != 1)
		{
			LOG_ERRORT(_iSocketFD, "ProxyHttps::Open - Cannot load extra certificate (SSL_CTX_add_extra_chain_cert)");
			CException::Throw(CException::OpenSSLError, "Cannot load extra certificate (SSL_CTX_add_extra_chain_cert)");
		}
	}
}

void ProxyHttps::Close()
{
	LOG_TRACE("ProxyHttps::Close");

	ProxyHttp::Close();

	if (_pSSLCtx != NULL)
		SSL_CTX_free(_pSSLCtx);

	_pSSLCtx = NULL;
}


void ProxyHttps::Accept(SessionWorkerPool& pool)
{
	LOG_TRACE("ProxyHttps::Accept");

	int iClientSocketFD = _pSocketConn->Accept(_iSocketFD);

	if (iClientSocketFD <= 0)
	{
		LOG_ERROR("ProxyHttps::Accept - Error while accepting client");
		return;
	}

	LOG_DEBUGF("ProxyHttps::Accept - Client connected [Socket: %i]", iClientSocketFD);

	SSL* pSSL = SSL_new(_pSSLCtx);

	if (pSSL == NULL)
	{
		LOG_ERRORT(_iSocketFD, "ProxyHttps::Accept - Cannot create SSL connection (SSL_new)");
		CException::Throw(CException::OpenSSLError, "Cannot create SSL connection (SSL_new)");
	}

	if (SSL_set_fd(pSSL, iClientSocketFD) == 0)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::Open - File descriptor set failed (SSL_set_fd)");
		CException::Throw(CException::OpenSSLError, "File descriptor set failed (SSL_set_fd)");
	}

	int res = SSL_accept(pSSL);

	int err  = SSL_get_error(pSSL, res);

	if (res != 1)
	{
		LOG_ERRORF("ProxyHttps::Accept - Error while accepting client (SSL_accept). Error: %i", err);

		if (pSSL != NULL)
		{
			if (SSL_shutdown(pSSL) == -1)
				LOG_ERROR("ProxyHttps::Accept - Error in SSL_shutdown");

			SSL_free(pSSL);
		}

		pSSL = NULL;
	}
	else
		pool.EnqueueSession(new Session(iClientSocketFD, pSSL, _info));

	ERR_remove_state(0);
}
