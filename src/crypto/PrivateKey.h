#pragma once

#include "Radford.h"

#include <openssl/evp.h>

class PrivateKey
{

public:

	PrivateKey();
	~PrivateKey();

	void ImportPEM(const char* filename, const char* password = NULL);
	void ImportPEM(const char* data, const size_t& dataLen, const char* password = NULL);

	void FreeKey();

	EVP_PKEY* GetKeyEVP() { return _pEVPKey; }
	const EVP_PKEY* GetKeyEVP() const { return _pEVPKey; }

private:

	void Import(BIO* b, const char* password);

	EVP_PKEY* _pEVPKey;

};