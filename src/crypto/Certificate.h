#pragma once

#include "Radford.h"

#include <openssl/x509.h>

class Certificate
{

public:

	Certificate();
	~Certificate();

	void ImportPEM(const char* filename);
	void ImportPEM(const char* data, const size_t& dataLen);

	void FreeCert();

	X509* GetX509() { return _pX509; }
	const X509* GetX509() const { return _pX509; }

private:

	void Import(BIO* b);

	X509* _pX509;

};

typedef std::vector<Certificate*> CertificatePtrStore;
