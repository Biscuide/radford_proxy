#include "crypto/PrivateKey.h"
#include "framework/Logger.h"

#include <openssl/pem.h>
#include <openssl/err.h>

PrivateKey::PrivateKey() : _pEVPKey(NULL)
{
	LOG_TRACE("PrivateKey::PrivateKey");
}

PrivateKey::~PrivateKey()
{
	LOG_TRACE("PrivateKey::~PrivateKey");

	FreeKey();
}

void PrivateKey::ImportPEM(const char* filename, const char* password)
{
	LOG_TRACE("PrivateKey::ImportPEM");

	if (filename == NULL)
	{
		LOG_ERROR("PrivateKey::ImportPEM - filename = NULL");
		CException::Throw(CException::ParamError, "filename = NULL");
	}

	size_t filenameLen = strlen(filename);
	if (filenameLen == 0)
	{
		LOG_ERROR("PrivateKey::ImportPEM - filenameLen = 0");
		CException::Throw(CException::ParamError, "filenameLen = 0");
	}

	LOG_DEBUGF("PrivateKey::ImportPEM - Filename: \"%s\"", filename);

	BIO* pBioKeyFile = BIO_new_file(filename, "rb");

	if (pBioKeyFile == NULL)
	{
		LOG_ERROR("PrivateKey::ImportPEM - pBioKey == NULL");
		CException::Throw(CException::OpenSSLError, "pBioKey == NULL");
	}

	Import(pBioKeyFile, password);
}

void PrivateKey::ImportPEM(const char* data, const size_t& dataLen, const char * password)
{
	LOG_TRACE("PrivateKey::ImportPEM");

	if (data == NULL)
	{
		LOG_ERROR("PrivateKey::ImportPEM - data = NULL");
		CException::Throw(CException::ParamError, "data = NULL");
	}

	if (dataLen == 0)
	{
		LOG_ERROR("PrivateKey::ImportPEM - dataLen = 0");
		CException::Throw(CException::ParamError, "dataLen = 0");
	}

	BIO* pBioKey = BIO_new_mem_buf((void*)data, dataLen);
	if (pBioKey == NULL)
	{
		LOG_ERROR("PrivateKey::ImportPEM - pBioKey == NULL");
		CException::Throw(CException::OpenSSLError, "pBioKey == NULL");
	}

	Import(pBioKey, password);
}


void PrivateKey::FreeKey()
{
	LOG_TRACE("PrivateKey::FreeKey");

	if (_pEVPKey)
	{
		EVP_PKEY_free(_pEVPKey);
		_pEVPKey = NULL;
	}
}

void PrivateKey::Import(BIO* b, const char* password)
{
	LOG_TRACE("PrivateKey::Import");

	FreeKey();
	_pEVPKey = PEM_read_bio_PrivateKey(b, NULL, NULL, (void*)password);

	if (_pEVPKey == NULL)
	{
		BIO_free(b);

		LOG_ERRORF("PrivateKey::ImportPEM - Cannot read private key (error: %i).", ERR_GET_REASON(ERR_peek_error()));
		CException::Throw(CException::OpenSSLError, "Cannot read private key");
	}

	BIO_free(b);
}
