#include "crypto/Certificate.h"
#include "framework/Logger.h"

#include <openssl/pem.h>
#include <openssl/err.h>

Certificate::Certificate() : _pX509(NULL)
{
	LOG_TRACE("Certificate::Certificate");
}

Certificate::~Certificate()
{
	LOG_TRACE("Certificate::~Certificate");

	FreeCert();
}

void Certificate::ImportPEM(const char* filename)
{
	LOG_TRACE("Certificate::ImportPEM");

	if (filename == NULL)
	{
		LOG_ERROR("Certificate::ImportPEM - filename = NULL");
		CException::Throw(CException::ParamError, "filename = NULL");
	}

	size_t filenameLen = strlen(filename);
	if (filenameLen == 0)
	{
		LOG_ERROR("Certificate::ImportPEM - filenameLen = 0");
		CException::Throw(CException::ParamError, "filenameLen = 0");
	}

	LOG_DEBUGF("Certificate::ImportPEM - Filename: \"%s\"", filename);

	BIO* pBioCertFile = BIO_new_file(filename, "rb");

	if (pBioCertFile == NULL)
	{
		LOG_ERROR("Certificate::ImportPEM - pBioCertFile == NULL");
		CException::Throw(CException::OpenSSLError, "pBioCertFile == NULL");
	}

	Import(pBioCertFile);
}

void Certificate::ImportPEM(const char* data, const size_t& dataLen)
{
	LOG_TRACE("Certificate::ImportPEM");

	if (data == NULL)
	{
		LOG_ERROR("Certificate::ImportPEM - data = NULL");
		CException::Throw(CException::ParamError, "data = NULL");
	}

	if (dataLen == 0)
	{
		LOG_ERROR("Certificate::ImportPEM - dataLen = 0");
		CException::Throw(CException::ParamError, "dataLen = 0");
	}

	BIO* pBioCert = BIO_new_mem_buf((void*)data, dataLen);
	if (pBioCert == NULL)
	{
		LOG_ERROR("Certificate::ImportPEM - pBioKey == NULL");
		CException::Throw(CException::OpenSSLError, "pBioKey == NULL");
	}

	Import(pBioCert);
}

void Certificate::FreeCert()
{
	LOG_TRACE("Certificate::FreeCert");

	if (_pX509)
	{
		X509_free(_pX509);
		_pX509 = NULL;
	}
}

void Certificate::Import(BIO* b)
{
	LOG_TRACE("Certificate::Import");

	FreeCert();
	_pX509 = PEM_read_bio_X509(b, NULL, NULL, NULL);

	if (_pX509 == NULL)
	{
		BIO_free(b);

		LOG_ERRORF("Certificate::Import - Cannot read certificate (error: %i).", ERR_GET_REASON(ERR_peek_error()));
		CException::Throw(CException::OpenSSLError, "Cannot read certificate");
	}

	BIO_free(b);
}
