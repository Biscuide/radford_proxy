#pragma once

#include "Radford.h"
#include "crypto/PrivateKey.h"
#include "crypto/Certificate.h"

class DataMgr
{

public:

	inline static DataMgr* Instance() { return _instance; }

	static void Initialize();
	static void Deinitialize();

	Certificate* GetEndCert() const { return _endCert; }
	PrivateKey* GetEndCertPK() const { return _endCertPK; }
	const CertificatePtrStore& GetExtraCerts() const { return _extraCerts; }
	const CertificatePtrStore& GetTrustCerts() const { return _trustCerts; }
	const std::string& GetHttpIdentificatorSecretKey() const { return _httpIdentificatorSecretKey; }

private:

	void LoadData();
	void ReleaseData();

	DataMgr();
	~DataMgr();

	Certificate* _endCert;
	PrivateKey* _endCertPK;
	CertificatePtrStore _extraCerts;
	CertificatePtrStore _trustCerts;
	std::string _httpIdentificatorSecretKey;

	static DataMgr* _instance;
};

#define DATAMGR DataMgr::Instance()
