#include "http/HttpHeader.h"
#include "http/HttpClient.h"

HttpHeader::HttpHeader(const int* fd/* = NULL*/) : _uHeaderSize(0), _iSocketFD(fd ? *fd : SOCKET_UNDEF)
{

}

HttpHeader::~HttpHeader()
{

}

void HttpHeader::Reset()
{
	_iSocketFD = SOCKET_UNDEF;
	_uHeaderSize = 0;
	_fields.clear();
}

void HttpHeader::HeaderBufferAppend(const byte* data, const size_t& len)
{
	LOG_TRACET(_iSocketFD, "HttpHeader::HeaderBufferAppend");

	LOG_DEBUGFT(_iSocketFD, "HttpHeader::HeaderBufferAppend - Len: %lu", len);

	_vHeaderBuffer.insert(_vHeaderBuffer.end(), data, data + len);
}

bool HttpHeader::Parse()
{
	LOG_TRACET(_iSocketFD, "HttpHeader::Parse");

	if (_vHeaderBuffer.empty())
	{
		LOG_ERRORT(_iSocketFD, "HttpHeader::Parse - Buffer is empty");
		return false;
	}

	int socketTmp = _iSocketFD;
	Reset();
	_iSocketFD = socketTmp;

	const char* ptrBegin = (char*)_vHeaderBuffer.data();
	const char* ptrEnd = (char*)_vHeaderBuffer.data() + _vHeaderBuffer.size();
	char* ptrFieldBegin = (char*)_vHeaderBuffer.data();
	char* ptrCurr = ptrFieldBegin;
	size_t count = 0;

	while (ptrCurr < ptrEnd)
	{
		if (*ptrCurr == '\n')
		{
			// End of header
			if (ptrCurr - ptrFieldBegin - 1 <= 0)
			{
				_uHeaderSize = ptrCurr - ptrBegin + 1;
				LOG_DEBUGFT(_iSocketFD, "HttpHeader::Parse - End of header, header size: %lu", _uHeaderSize);
				if (_uHeaderSize < _vHeaderBuffer.size())
					_vHeaderBuffer.erase(_vHeaderBuffer.begin(), _vHeaderBuffer.begin() + _uHeaderSize);
				else
				{
					if (_uHeaderSize > _vHeaderBuffer.size())
						LOG_ERRORFT(_iSocketFD, "HttpHeader::Parse - Header size is bigger than internal data buffer: %lu", _vHeaderBuffer.size());

					_vHeaderBuffer.clear();
				}

				return true;
			}

			std::string tmp;
			tmp.append(ptrFieldBegin, ptrCurr - ptrFieldBegin - 1);

			if (count == 0)
				ParseFirstLine(tmp);
			else
			{
				size_t posDel = tmp.find(":");
				if (posDel < tmp.size())
				{
					std::string fieldVal = tmp.substr(posDel + 2, tmp.size() - posDel - 2);

					//if (tmp.substr(0, posDel) == "Host")
					//{
					//	const std::string oldStr = "progtest2.fit.cvut.cz";
					//	const std::string newStr = "progtest.fit.cvut.cz";
					//	size_t pos = fieldVal.find(oldStr);

					//	if (pos != std::string::npos)
					//		fieldVal.replace(pos, oldStr.length(), newStr);
					//}

					AddField(tmp.substr(0, posDel), fieldVal);
				}
			}

			ptrFieldBegin = ptrCurr + 1;
			count++;
		}

		ptrCurr++;
	}

	return false;
}

void HttpHeader::BuildHeader(std::string& res) const
{
	LOG_TRACET(_iSocketFD, "HttpHeader::BuildHeader");

	BuildFirstLine(res);

	for (VectorStringPair::const_iterator itr = _fields.begin(); itr != _fields.end(); itr++)
	{
		res.append((*itr).first + ": " + (*itr).second + "\r\n");
	}

	res.append("\r\n");
}

bool HttpHeader::GetFieldValue(const std::string& fieldName, std::string& fieldValue) const
{
	LOG_TRACET(_iSocketFD, "HttpHeader::GetFieldValue");

	for (VectorStringPair::const_iterator itr = _fields.begin(); itr != _fields.end(); itr++)
	{
		if ((*itr).first == fieldName)
		{
			fieldValue = (*itr).second;
			return true;
		}
	}

	return false;
}

void HttpHeader::InsertIdentificationData(const VectorStringPair& data)
{
	for (VectorStringPair::const_iterator itr = data.begin(); itr != data.end(); itr++)
	{
		AddField((*itr).first, (*itr).second);
	}
}

void HttpHeader::AddField(const std::string & name, const std::string & value)
{
	_fields.push_back(StringPair(name, value));
}
