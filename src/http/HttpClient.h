#pragma once

#include "Radford.h"
#include "http/HttpRequest.h"
#include "http/HttpResponse.h"
#include "socket/SocketConnector.h"

#define HTTP_PORT				80
#define HTTPS_PORT				443

#define ASCII_CR				0x0D
// 2x "\r\n"
#define CHUNK_BORDER_SIZE		4

#define BUFFER_SIZE				4096

#define CONNECTION_CHECK_PERIOD	1

#define RESP_CODE_BAD_REQUEST			400
#define RESP_CODE_SERVER_NYI			501
#define RESP_CODE_SERVER_BAD_GATEWAY	502
#define RESP_CODE_SUCCES_OK				200
#define HTTPv1_1_PROTOCOL		"HTTP/1.1"

typedef enum ProtocolName
{
	PROTOCOL_HTTP,
	PROTOCOL_HTTPS,
	PROTOCOL_MAX
} ProtocolName;

std::string GetProtocolName(const ProtocolName& pn);

class HttpClient
{

public:

	typedef enum ResendStatus
	{
		RS_UNK			= 0x00,
		RS_CLOSED		= 0x01,
		RS_CLOSE		= 0x02,
		RS_KEEP_ALIVE	= 0x04,
		RS_ERROR		= 0x08,
		RS_PROXY_RESP	= 0x10,
		RS_TUNNELING	= 0x20
	} ResendStatus;

	HttpClient(const int* fd = NULL);
	virtual ~HttpClient();

	virtual void Open(const std::string& sAddress, const uint16& uPort);
	virtual void Close();
	virtual bool IsClosed() const { return _iSocketFD == SOCKET_UNDEF; }

	// bool ReceiveRequest();
	void SendRequestHeader(const HttpRequest* pRequest);
	uint16 ResendRequest(HttpClient* pServer, const uint16& uPort, HttpResponse* proxyResponse);
	bool ResendData(HttpClient* pServer);

	// bool ReceiveResponse();
	void SendResponseHeader(const HttpResponse* pResponse);
	uint16 ResendResponse(HttpClient* pClient);

	const int GetSocketFD() const { return _iSocketFD; }
	const HttpResponse* GetResponse() const { return _pResponse; }
	const HttpRequest* GetRequest() const { return _pRequest; }

	const std::string& GetHost() const { return _sHost; }
	const uint16& GetPort() const { return _uPort; }

protected:

	virtual int Read(byte* buffer, const size_t& bufferSize);
	virtual int Write(const byte* buffer, const size_t& bufferSize);

	void SendData(const byte* data, const size_t& len);
	bool LoadChunkSize(const byte* data, const size_t& dataSize, uint32& iMessageBodySize);

	void ParseURI(const std::string& uri, std::string& host, uint16& uPort);

	bool SendMessageBodyChunked(const byte* buffer, size_t dataLeftSize, uint32& iMessageBodySize);
	bool SendMessageBodyNormal(const byte* buffer, size_t dataLeftSize, uint32& uMessageBodySize);

	void GenerateResponse(const uint16& statusCode, const std::string& reason, HttpResponse* res);

protected:

	int _iSocketFD;
	std::string _sHost;
	uint16 _uPort;
	SocketConnector* _pSocketConn;
	HttpResponse* _pResponse;
	HttpRequest* _pRequest;

};