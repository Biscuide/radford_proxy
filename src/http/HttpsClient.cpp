#include "http/HttpsClient.h"
#include "DataMgr.h"

#include <openssl/err.h>
#include <openssl/crypto.h>

#undef X509_NAME

#include <openssl/x509.h>
#include <openssl/x509v3.h>

#ifdef WIN32
#include <chrono>
#include <thread>
#else
#include <unistd.h>
#endif

HttpsClient::HttpsClient(SSL* pSSL/* = NULL*/, const int* fd/* = NULL*/) : HttpClient(fd), _pSSL(pSSL), _pSSLCtx(NULL)
{
	LOG_TRACET(_iSocketFD, "HttpsClient::HttpsClient");
}

HttpsClient::~HttpsClient()
{
	LOG_TRACET(_iSocketFD, "HttpsClient::~HttpsClient");

	if (_pSSL != NULL || _pSSLCtx != NULL || _iSocketFD != SOCKET_UNDEF)
		Close();
}

void HttpsClient::Open(const std::string& sAddress, const uint16& uPort)
{
	LOG_TRACET(_iSocketFD, "HttpsClient::Open");

	HttpClient::Open(sAddress, uPort);

	// Context create
	_pSSLCtx = SSL_CTX_new(SSLv23_client_method());
	if (_pSSLCtx == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::Open - Cannot create SSL context (SSL_CTX_new)");
		CException::Throw(CException::OpenSSLError, "Cannot create SSL context (SSL_CTX_new)");
	}

	// Disable protocols
	SSL_CTX_set_options(_pSSLCtx, SSL_OP_NO_TLSv1 | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3);

	// Nacteni duveryhodnych ulozist a certifikatu
	X509_STORE* pTrustStore = SSL_CTX_get_cert_store(_pSSLCtx);
	LoadTrustStore(pTrustStore);

	// Create SSL connection
	_pSSL = SSL_new(_pSSLCtx);
	if (_pSSL == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::Open - Cannot create SSL connection (SSL_new)");
		CException::Throw(CException::OpenSSLError, "Cannot create SSL connection (SSL_new)");
	}

	// Set file descriptor
	if (SSL_set_fd(_pSSL, _iSocketFD) == 0)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::Open - File descriptor set failed (SSL_set_fd)");
		CException::Throw(CException::OpenSSLError, "File descriptor set failed (SSL_set_fd)");
	}

	// SSL pripojeni na server
	int err = SSL_connect(_pSSL);
	if (err != 1)
	{
		LOG_ERRORFT(_iSocketFD, "HttpsClient::Open - SSL connection was not establish successfully (SSL_connect, err: %i)", SSL_get_error(_pSSL, err));
		CException::Throw(CException::OpenSSLError, "SSL connection was not establish successfully (SSL_connect)");
	}

	ERR_remove_state(0);

	X509* pPeerCert = NULL;
	X509* pPeerCertIssuer = NULL;
	// Ziskani chainu SSL certifikatu ze serveru
	STACK_OF(X509)* pPeerCertChain = NULL;

	// Overeni SSL certifikatu serveru
	try
	{
		if (CONFIG->GetBoolValue(SSL_VERIFY_WHOLE_CHAIN_CUSTOM))
			pPeerCertChain = GetPeerCertificateChain();

		if (pPeerCertChain == NULL)
		{
			// Ziskani SSL certifikatu ze serveru
			pPeerCert = GetPeerCertificate();

			// Overeni koncoveho certifikatu
			int res = SSL_get_verify_result(_pSSL);
			if (res != X509_V_OK)
			{
				LOG_ERRORFT(_iSocketFD, "HttpsClient::Open - Error while verifying certificate (SSL_get_verify_result - %i)", res);
				CException::Throw(CException::OpenSSLError, "Error while verifying certificate (SSL_get_verify_result)");
			}
		}
		else
		{
			X509* pPeerCertTmp = sk_X509_value(pPeerCertChain, 0);
			if (pPeerCertTmp == NULL)
			{
				LOG_ERRORT(_iSocketFD, "HttpsClient::Open - pPeerCertTmp = NULL");
				CException::Throw(CException::OpenSSLError, "pPeerCertTmp = NULL");
			}

			pPeerCert = X509_dup(pPeerCertTmp);

			if (pPeerCert == NULL)
			{
				LOG_ERRORT(_iSocketFD, "HttpsClient::Open - pPeerCert == NULL");
				CException::Throw(CException::OpenSSLError, "pPeerCert == NULL");
			}

			if (sk_X509_num(pPeerCertChain) > 1)
			{
				X509* pPeerCertIssuerTmp = sk_X509_value(pPeerCertChain, 1);
				if (pPeerCertIssuerTmp == NULL)
				{
					LOG_ERRORT(_iSocketFD, "HttpsClient::Open - pPeerCertIssuerTmp = NULL");
					CException::Throw(CException::OpenSSLError, "pPeerCertIssuerTmp = NULL");
				}

				pPeerCertIssuer = X509_dup(pPeerCertIssuerTmp);

				if (pPeerCertIssuer == NULL)
				{
					LOG_ERRORT(_iSocketFD, "HttpsClient::Open - pPeerCertIssuer == NULL");
					CException::Throw(CException::OpenSSLError, "pPeerCertIssuer == NULL");
				}
			}

			// Cas overeni - aktualni
			time_t verifyTime = 0;
			time(&verifyTime);

			// Overeni koncoveho certifikatu
			VerifyCertificate(pPeerCert, verifyTime, pPeerCertIssuer);

			// Overeni retezce certifikatu
			VerifyCertificateChain(pPeerCert, pPeerCertChain, verifyTime);
		}
	}
	catch (...)
	{
		if (pPeerCert != NULL)
			X509_free(pPeerCert);

		if (pPeerCertIssuer != NULL)
			X509_free(pPeerCertIssuer);

		throw;
	}

	if (pPeerCert != NULL)
		X509_free(pPeerCert);

	if (pPeerCertIssuer != NULL)
		X509_free(pPeerCertIssuer);
}

void HttpsClient::Close()
{
	LOG_TRACET(_iSocketFD, "HttpsClient::Close");

	if (_pSSL != NULL)
	{
		int res = SSL_shutdown(_pSSL);
		if (res == -1)
		{
			LOG_ERRORFT(_iSocketFD, "HttpsClient::Close - Error in SSL_shutdown. Error: %i", SSL_get_error(_pSSL, res));
		}
		else if (res == 0)
		{
			int counter = 0;
			while (SSL_shutdown(_pSSL) == 0 && counter++ < 5)
			{
#ifdef WIN32
				std::this_thread::sleep_for(std::chrono::microseconds(50));
#else
				usleep(50 * 1000);
#endif
			}
		}

		SSL_free(_pSSL);
	}

	if (_pSSLCtx != NULL)
		SSL_CTX_free(_pSSLCtx);

	_pSSLCtx = NULL;
	_pSSL = NULL;

	HttpClient::Close();
}

int HttpsClient::Read(byte* buffer, const size_t& bufferSize)
{
	int res = SSL_read(_pSSL, buffer, bufferSize);
	ERR_remove_state(0);
	return res;
}

int HttpsClient::Write(const byte* buffer, const size_t& bufferSize)
{
	return SSL_write(_pSSL, buffer, bufferSize);
}

X509* HttpsClient::GetPeerCertificate() const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::GetPeerCertificate");

	if (_pSSL == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetPeerCertificate - _pSSL == NULL");
		CException::Throw(CException::InternalError, "_pSSL == NULL");
	}

	X509* pCert = SSL_get_peer_certificate(_pSSL);

	if (pCert == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetPeerCertificate - Cannot get peer certificate (SSL_get_peer_certificate)");
		CException::Throw(CException::OpenSSLError, "Cannot get peer certificate (SSL_get_peer_certificate)");
	}

	return X509_dup(pCert);
}

STACK_OF(X509)* HttpsClient::GetPeerCertificateChain() const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::GetPeerCertificateChain");

	if (_pSSL == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetPeerCertificateChain - _pSSL == NULL");
		CException::Throw(CException::InternalError, "_pSSL == NULL");
	}

	STACK_OF(X509)* pCerts = SSL_get_peer_cert_chain(_pSSL);

	if (pCerts == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetPeerCertificateChain - Cannot get peer certificate chain (SSL_get_peer_cert_chain)");
		CException::Throw(CException::OpenSSLError, "Cannot get peer chain (SSL_get_peer_cert_chain)");
	}

	return pCerts;
}

void HttpsClient::LoadTrustStore(X509_STORE* pTrustStore) const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::LoadTrustStore");

	if (pTrustStore == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::LoadTrustStore - pTrustStore == NULL");
		CException::Throw(CException::InternalError, "pTrustStore == NULL");
	}

	std::string trustStorePath = "";

	if (CONFIG->GetBoolValue(SSL_USE_SYSTEM_TRUST_STORE))
	{
#ifdef UNIX
		trustStorePath = "/etc/ssl/certs";
#else
		trustStorePath = "";
#endif
	}
	else
		trustStorePath = CONFIG->GetStrValue(CERT_TRUST_CERT_FOLDER_PATH);

	LOG_DEBUGT(_iSocketFD, "HttpsClient::LoadTrustStore - Add trusted paths ...");

	// Nastaveni cesty ke slozce s duveryhodnymi certifikaty
	if (!trustStorePath.empty())
	{
		if (!X509_STORE_load_locations(pTrustStore, NULL, trustStorePath.c_str()))
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::LoadTrustStore - Error loading trust store (X509_STORE_load_locations)");
			CException::Throw(CException::OpenSSLError, "Error loading trust store (X509_STORE_load_locations)");
		}
	}

	// Nastaveni doplnujicich duveryhodnych certifikatu
	LOG_DEBUGT(_iSocketFD, "HttpsClient::LoadTrustStore - Add trusted certs ...");

	const CertificatePtrStore& trustCerts = DATAMGR->GetTrustCerts();
	for (CertificatePtrStore::const_iterator itr = trustCerts.begin(); itr != trustCerts.end(); itr++)
	{
		if (*itr == NULL || (*itr)->GetX509() == NULL)
		{
			LOG_WARNINGT(_iSocketFD, "HttpsClient::LoadTrustStore - Expected trusted cert is not set!");
		}
		else
			X509_STORE_add_cert(pTrustStore, (*itr)->GetX509());
	}
}

void HttpsClient::VerifyCertificate(X509* pCert, const time_t& verifyTime, X509* pCertIssuer/* = NULL*/) const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::VerifyCertificate");

	if (pCert == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificate - pCert == NULL");
		CException::Throw(CException::InternalError, "pCert == NULL");
	}

	std::string certSubjectName = GetCertificateSubjectName(pCert);
	std::string certIssuerName = GetCertificateIssuerName(pCert);

	//LOG_DEBUGFT(_iSocketFD, "Certificate subject name: %s", certSubjectName.c_str());
	//LOG_DEBUGFT(_iSocketFD, "Certificate issuer name: %s", certIssuerName.c_str());

	// Verify validity
	{
		if (X509_cmp_time(X509_get_notBefore(pCert), (time_t*)&verifyTime) > 0)
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificate - The peer certificate is not valid yet.");
			CException::Throw(CException::CertificateVerifyError, "The peer certificate is not valid yet.");
		}

		if (X509_cmp_time(X509_get_notAfter(pCert), (time_t*)&verifyTime) < 0)
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificate - The peer certificate is expired.");
			CException::Throw(CException::CertificateVerifyError, "The peer certificate is expired.");
		}
	}

	// Verify signature
	if (pCertIssuer != NULL)
	{
		EVP_PKEY* pIssuerPK = X509_get_pubkey(pCertIssuer);

		if (pIssuerPK == NULL)
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificate - Cannot get public key of issuer.");
			CException::Throw(CException::OpenSSLError, "Cannot get public key of issuer.");
		}

		int res = X509_verify(pCert, pIssuerPK);

		if (res != 1)
		{
			LOG_ERRORFT(_iSocketFD, "HttpsClient::VerifyCertificate - Error while verifying certificate signature (%i).", res);
			CException::Throw(CException::CertificateVerifyError, "Error while verifying certificate signature.");
		}

		EVP_PKEY_free(pIssuerPK);
	}
}

void HttpsClient::VerifyCertificateChain(X509* pCert, STACK_OF(X509)* pCertChain, const time_t& verifyTime) const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::VerifyCertificateChain");

	if (pCert == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pCert == NULL");
		CException::Throw(CException::InternalError, "pCert == NULL");
	}

	if (pCertChain == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pCertChain == NULL");
		CException::Throw(CException::InternalError, "pCertChain == NULL");
	}

	if (sk_X509_num(pCertChain) < 1)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pCertChain is empty");
		CException::Throw(CException::InternalError, "pCertChain is empty");
	}

	X509_STORE_CTX* pStoreCtx = NULL;
	X509_STORE* pTrustStore = NULL;
	STACK_OF(X509)* pExtraCerts = NULL;
	X509* pTrustCert = NULL;

	try
	{
		pStoreCtx = X509_STORE_CTX_new();
		if (pStoreCtx == NULL)
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pStoreCtx == NULL");
			CException::Throw(CException::OpenSSLError, "pStoreCtx == NULL");
		}

		pTrustStore = X509_STORE_new();
		if (pTrustStore == NULL)
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pTrustStore == NULL");
			CException::Throw(CException::OpenSSLError, "pTrustStore == NULL");
		}

		// Nacteni duveryhodnych ulozist a certifikatu
		LoadTrustStore(pTrustStore);

		// Chain
		if (sk_X509_num(pCertChain) > 2)
		{
			pExtraCerts = sk_X509_new_null();

			if (pExtraCerts == NULL)
			{
				LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pExtraCerts == NULL");
				CException::Throw(CException::OpenSSLError, "pExtraCerts == NULL");
			}

			for (int i = 1; i < sk_X509_num(pCertChain) - 1; i++)
			{
				X509* pCertTmp = sk_X509_value(pCertChain, i);
				if (pCertTmp == NULL)
				{
					LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificateChain - pCertTmp = NULL");
					CException::Throw(CException::OpenSSLError, "pCertTmp = NULL");
				}

				X509* pCertDup = X509_dup(pCertTmp);

				if (pCertDup == NULL)
				{
					LOG_ERRORT(_iSocketFD, "HttpsClient::Open - pCertDup == NULL");
					CException::Throw(CException::OpenSSLError, "pCertDup == NULL");
				}

				if (!sk_X509_push(pExtraCerts, pCertDup))
				{
					LOG_ERRORT(_iSocketFD, "HttpsClient::Open - Cannot push cert into extra cert store");
					CException::Throw(CException::OpenSSLError, "Cannot push cert into extra cert store");
				}
			}
		}

		// Inicializace
		if (X509_STORE_CTX_init(pStoreCtx, pTrustStore, pCert, pExtraCerts) != 1)
		{
			LOG_ERRORT(_iSocketFD, "HttpsClient::Open - Cannot initialize store context (X509_STORE_CTX_init)");
			CException::Throw(CException::OpenSSLError, "Cannot initialize store context (X509_STORE_CTX_init)");
		}

		//unsigned long nFlags = X509_V_FLAG_CRL_CHECK_ALL;
		unsigned long nFlags = 0;
		X509_STORE_CTX_set_flags(pStoreCtx, nFlags);
		X509_STORE_CTX_set_time(pStoreCtx, nFlags, verifyTime);
		X509_STORE_CTX_set_purpose(pStoreCtx, X509_PURPOSE_SSL_SERVER);

		// Overeni certifikacniho retezce
		int nResult = X509_verify_cert(pStoreCtx);

		if (nResult != 1)
		{
			LOG_ERRORFT(_iSocketFD, "HttpsClient::Open - Error while verifying certificate chain (%i)", ERR_get_error());
			CException::Throw(CException::OpenSSLError, "Error while verifying certificate chain");
		}
	}
	catch (...)
	{
		if (pTrustCert != NULL)
			X509_free(pTrustCert);

		if (pExtraCerts != NULL)
			sk_X509_pop_free(pExtraCerts, X509_free);

		if (pTrustStore != NULL)
			X509_STORE_free(pTrustStore);

		if (pStoreCtx != NULL)
			X509_STORE_CTX_free(pStoreCtx);

		throw;
	}

	if (pTrustCert != NULL)
		X509_free(pTrustCert);

	if (pExtraCerts != NULL)
		sk_X509_pop_free(pExtraCerts, X509_free);

	if (pTrustStore != NULL)
		X509_STORE_free(pTrustStore);

	if (pStoreCtx != NULL)
		X509_STORE_CTX_free(pStoreCtx);
}

std::string HttpsClient::GetCertificateSubjectName(X509* pCert) const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::GetCertificateSubjectName");

	if (pCert == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::VerifyCertificate - pCert == NULL");
		CException::Throw(CException::InternalError, "pCert == NULL");
	}

	X509_NAME* pCertNameX509 = X509_get_subject_name(pCert);
	if (pCertNameX509 == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetCertificateSubjectName - Cannot get certificate subject name");
		CException::Throw(CException::OpenSSLError, "Cannot get certificate subject name");
	}

	char* pCertName = X509_NAME_oneline(pCertNameX509, 0, 0);
	if (pCertName == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetCertificateSubjectName - Error while conversing certificate subject name");
		CException::Throw(CException::OpenSSLError, "Error while conversing certificate subject name");
	}

	std::string res = pCertName;

	OPENSSL_free(pCertName);

	return res;
}

std::string HttpsClient::GetCertificateIssuerName(X509* pCert) const
{
	LOG_TRACET(_iSocketFD, "HttpsClient::GetCertificateIssuerName");

	if (pCert == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetCertificateIssuerName - pCert == NULL");
		CException::Throw(CException::InternalError, "pCert == NULL");
	}

	X509_NAME* pCertNameX509 = X509_get_issuer_name(pCert);
	if (pCertNameX509 == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetCertificateIssuerName - Cannot get certificate subject name");
		CException::Throw(CException::OpenSSLError, "Cannot get certificate subject name");
	}

	char* pCertName = X509_NAME_oneline(pCertNameX509, 0, 0);
	if (pCertName == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpsClient::GetCertificateIssuerName - Error while conversing certificate subject name");
		CException::Throw(CException::OpenSSLError, "Error while conversing certificate subject name");
	}

	std::string res = pCertName;

	OPENSSL_free(pCertName);

	return res;
}
