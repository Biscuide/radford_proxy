#pragma once

#include "http/HttpClient.h"

#include <openssl/ssl.h>

class HttpsClient : public HttpClient
{

public:

	HttpsClient(SSL* pSSL = NULL, const int* fd = NULL);
	virtual ~HttpsClient();

	virtual void Open(const std::string& sAddress, const uint16& uPort);
	virtual void Close();
	virtual bool IsClosed() const { return _iSocketFD == SOCKET_UNDEF; }

private:

	virtual int Read(byte* buffer, const size_t& bufferSize);
	virtual int Write(const byte* buffer, const size_t& bufferSize);

	X509* GetPeerCertificate() const;
	STACK_OF(X509)* GetPeerCertificateChain() const;

	void LoadTrustStore(X509_STORE* pTrustStore) const;
	void VerifyCertificate(X509* pCert, const time_t& verifyTime, X509* pCertIssuer = NULL) const;
	void VerifyCertificateChain(X509* pCert, STACK_OF(X509)* pCertChain, const time_t& verifyTime) const;

	std::string GetCertificateSubjectName(X509* pCert) const;
	std::string GetCertificateIssuerName(X509* pCert) const;

	SSL* _pSSL;
	SSL_CTX* _pSSLCtx;
};
