#include "HttpRequest.h"

#include <string>
#include <stdio.h>
#include <algorithm>

std::vector<std::string> HttpRequest::_enabledHosts;

bool HttpRequest::HasMessageBody(const HttpRequest* pReq)
{
	switch (pReq->GetMethod())
	{
		case M_POST:
		case M_PUT:
			return true;
		default:
			return false;
	}

	return false;
}

HttpRequest::HttpRequest(const int* fd/* = NULL*/) : HttpHeader(fd), _method(REQ_METHOD_UNK)
{
	LOG_TRACET(_iSocketFD, "HttpRequest::HttpRequest");
}

HttpRequest::~HttpRequest()
{
	LOG_TRACET(_iSocketFD, "HttpRequest::~HttpRequest");
}

void HttpRequest::Reset()
{
	LOG_TRACET(_iSocketFD, "HttpRequest::Reset");

    _method = REQ_METHOD_UNK;
    for (uint8 i = 0; i < REQ_LINE_MAX; i++)
    {
        _requestLine[i] = "";
    }

	HttpHeader::Reset();
}

void HttpRequest::ParseFirstLine(const std::string& line)
{
	LOG_TRACET(_iSocketFD, "HttpRequest::ParseFirstLine");

	size_t pos = 0;
	size_t posLast = 0;
	uint8 i = 0;

	do
	{
		pos = line.find_first_of(" ", posLast);

		if (pos == std::string::npos)
		{
			pos = line.size();
			_requestLine[i] = line.substr(posLast, pos - posLast);
			LOG_DEBUGFT(_iSocketFD, "HttpRequest::ParseFirstLine - [%u] \"%s\"", i, _requestLine[i].c_str());
			i++;
			break;
		}

		_requestLine[i] = line.substr(posLast, pos - posLast);
		LOG_DEBUGFT(_iSocketFD, "HttpRequest::ParseFirstLine - [%u] \"%s\"", i, _requestLine[i].c_str());

		posLast = pos + 1;
	} while (++i < REQ_LINE_MAX);

	ParseMethod(_requestLine[REQ_LINE_METHOD]);
}

void HttpRequest::ParseMethod(const std::string& str)
{
	LOG_TRACET(_iSocketFD, "HttpRequest::ParseMethod");

    if (str == "GET")
        _method = M_GET;
    else if (str == "HEAD")
        _method = M_HEAD;
    else if (str == "POST")
        _method = M_POST;
    else if (str == "PUT")
        _method = M_PUT;
    else if (str == "DELETE")
        _method = M_DELETE;
    else if (str == "CONNECT")
        _method = M_CONNECT;
    else if (str == "OPTIONS")
        _method = M_OPTIONS;
    else if (str == "TRACE")
        _method = M_TRACE;
    else
        _method = REQ_METHOD_UNK;

	LOG_DEBUGFT(_iSocketFD, "HttpRequest::ParseMethod - Method: \"%s\" (%u)", str.c_str(), _method);
}

const std::string HttpRequest::GetURL() const
{
    return GetHost() + _requestLine[REQ_LINE_URI];
}

const std::string HttpRequest::GetHost() const
{
    for (VectorStringPair::const_iterator itr = _fields.begin(); itr != _fields.end(); itr++)
    {
        if ((*itr).first == "Host")
            return (*itr).second;
    }

    return "";
}

void HttpRequest::SetRequestLine(const std::string& method, const std::string& uri, const std::string& protocol)
{
	_requestLine[REQ_LINE_METHOD] = method;
	_requestLine[REQ_LINE_URI] = uri;
	_requestLine[REQ_LINE_VERSION] = protocol;

	ParseMethod(_requestLine[REQ_LINE_METHOD]);
}

void HttpRequest::BuildFirstLine(std::string& res) const
{
	LOG_TRACET(_iSocketFD, "HttpRequest::BuildFirstLine");

    for (uint8 i = 0; i < REQ_LINE_MAX; i++)
    {
        res.append(_requestLine[i]);
        if (i != REQ_LINE_MAX - 1)
            res.append(" ");
    }

    res.append("\r\n");
}

inline bool static binarySearchString(const std::string& a, const std::string& b) { return a.compare(b); }

bool HttpRequest::HasDisabledHost(const HttpRequest* pReq)
{
	LOG_TRACET(pReq->_iSocketFD, "HttpRequest::HasDisabledHost");

	if (_enabledHosts.empty() && !CONFIG->GetStrValue(PROXY_ENABLED_HOSTS).empty())
	{
		std::string enabledHosts = CONFIG->GetStrValue(PROXY_ENABLED_HOSTS);
		size_t enabledHostsCount = std::count(enabledHosts.begin(), enabledHosts.end(), ';');
		if (!enabledHosts.empty())
			enabledHostsCount++;

		if (!enabledHosts.empty())
		{
			_enabledHosts.reserve(enabledHostsCount);

			size_t nextPos = 0;
			size_t actualPos = 0;
			do
			{
				nextPos = enabledHosts.find(";", actualPos);
				std::string enabledHost = enabledHosts.substr(actualPos, nextPos - actualPos);
				actualPos = nextPos + 1;

				_enabledHosts.insert(lower_bound(_enabledHosts.begin(), _enabledHosts.end(), enabledHost, binarySearchString), enabledHost);

			} while (nextPos != std::string::npos);
		}
	}

	if (!_enabledHosts.empty())
	{
		if (_enabledHosts.size() == 1 && _enabledHosts[0] == "*")
			return false;

		return !binary_search(_enabledHosts.begin(), _enabledHosts.end(), pReq->GetHost(), binarySearchString);
	}

	return true;
}