#include "HttpResponse.h"
#include "framework/Utils.h"

#include <string>
#include <stdio.h>

HttpResponse::HttpResponse(const int* fd/* = NULL*/) : HttpHeader(fd), _statusCodeGroup(RESP_CODE_UNK)
{
	LOG_TRACET(_iSocketFD, "HttpResponse::HttpResponse");
}

HttpResponse::~HttpResponse()
{
	LOG_TRACET(_iSocketFD, "HttpResponse::~HttpResponse");
}

void HttpResponse::Reset()
{
	LOG_TRACET(_iSocketFD, "HttpResponse::Reset");

    _statusCodeGroup = RESP_CODE_UNK;
	for (uint8 i = 0; i < RESP_LINE_MAX; i++)
	{
		_statusLine[i] = "";
	}

	HttpHeader::Reset();
}

void HttpResponse::SetResponseLine(const std::string& protocol, const uint16& code, const std::string& reason)
{
	_statusLine[RESP_LINE_VERSION] = protocol;

	if (Utils::lexical_cast<std::string, uint16>(code, _statusLine[RESP_LINE_STATUS]) == false)
		CException::Throw(CException::LexicalCastFailed, "Response status code from uint to string");

	_statusLine[RESP_LINE_REASON] = reason;

	ParseResponseCode(_statusLine[RESP_LINE_STATUS]);
}

void HttpResponse::ParseFirstLine(const std::string& line)
{
	LOG_TRACET(_iSocketFD, "HttpResponse::ParseFirstLine");

	size_t pos = 0;
	size_t posLast = 0;
	uint8 i = 0;

	do
	{
		if (i == RESP_LINE_REASON)
			pos = line.length();
		else
			pos = line.find_first_of(" ", posLast);

		if (pos == std::string::npos)
		{
			pos = line.size();
			_statusLine[i] = line.substr(posLast, pos - posLast);
			LOG_DEBUGFT(_iSocketFD, "HttpResponse::ParseFirstLine - [%u] \"%s\"", i, _statusLine[i].c_str());
			i++;
			break;
		}

		_statusLine[i] = line.substr(posLast, pos - posLast);
		LOG_DEBUGFT(_iSocketFD, "HttpResponse::ParseFirstLine - [%u] \"%s\"", i, _statusLine[i].c_str());

		posLast = pos + 1;
	} while (++i < RESP_LINE_MAX);

	ParseResponseCode(_statusLine[RESP_LINE_STATUS]);
}

void HttpResponse::ParseResponseCode(const std::string& str)
{
	LOG_TRACET(_iSocketFD, "HttpResponse::ParseResponseCode");

    if (str.length() != 3)
        _statusCodeGroup = RESP_CODE_UNK;
    if (str.c_str()[0] == '1')
        _statusCodeGroup = C_INFO;
    else if (str.c_str()[0] == '2')
        _statusCodeGroup = C_SUCESS;
    else if (str.c_str()[0] == '3')
        _statusCodeGroup = C_REDIRECT;
    else if (str.c_str()[0] == '4')
        _statusCodeGroup = C_CLIENT_ERR;
    else if (str.c_str()[0] == '5')
        _statusCodeGroup = C_SERVER_ERR;
    else
        _statusCodeGroup = RESP_CODE_UNK;

	LOG_DEBUGFT(_iSocketFD, "HttpResponse::ParseResponseCode - Status code: \"%s\" (%u)", str.c_str(), _statusCodeGroup);
}

void HttpResponse::BuildFirstLine(std::string& res) const
{
	LOG_TRACET(_iSocketFD, "HttpResponse::BuildFirstLine");

    for (uint8 i = 0; i < RESP_LINE_MAX; i++)
    {
        res.append(_statusLine[i]);
        if (i != RESP_LINE_MAX - 1)
            res.append(" ");
    }

    res.append("\r\n");
}
