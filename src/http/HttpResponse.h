#pragma once

#include "http/HttpHeader.h"
#include "Radford.h"

#include <stddef.h>

class HttpResponse : public HttpHeader
{

public:

	friend class HttpClient;

    typedef enum HttpRespStatusLine
    {
        RESP_LINE_VERSION,
        RESP_LINE_STATUS,
        RESP_LINE_REASON,
        RESP_LINE_MAX
    } HttpRespStatusLine;

    typedef enum HttpRespStatusCode
    {
        RESP_CODE_UNK,
        C_INFO,
        C_SUCESS,
        C_REDIRECT,
        C_CLIENT_ERR,
        C_SERVER_ERR,
        C_MAX
    } HttpRespStatusCode;

	HttpResponse(const int* fd = NULL);
	virtual ~HttpResponse();

    void Reset();

	const HttpRespStatusCode GetStatusCodeGroup() const { return _statusCodeGroup; }
	const std::string* GetStatusLine() const { return _statusLine; }

protected:

	void SetResponseLine(const std::string& protocol, const uint16& code, const std::string& reason);

private:

	void ParseFirstLine(const std::string& line);
	void BuildFirstLine(std::string& res) const;

    void ParseResponseCode(const std::string& str);

	HttpRespStatusCode _statusCodeGroup;
	std::string _statusLine[RESP_LINE_MAX];

};
