#pragma once

#include "http/identification/Identifier.h"

class HMACIdentifier : public Identifier
{

public:

	friend class Identifier;

protected:

	HMACIdentifier();
	virtual ~HMACIdentifier();

	virtual void CreateIdentificationData(VectorStringPair& data) const;
	virtual void SignIdentificationData(VectorStringPair& data, std::string& sign) const;

};