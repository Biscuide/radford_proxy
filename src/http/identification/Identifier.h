#pragma once

#include "Radford.h"
#include "http/HttpHeader.h"

typedef enum HttpHeaderIdentifiers {
	HttpIdentifierNone,
	HttpIdentifierHMAC,
	HttpIdentifierMax
} HttpHeaderIdentifiers;

typedef enum NetProtocolIdentifiers
{
	NET_PROTOCOL_IPV4,
	NET_PROTOCOL_IPV6
} NetProtocolIdentifiers;

class Identifier
{

public:

	static Identifier* CreateIdentifier(const int& fd);
	static void DestroyIdentifier(Identifier* pIdentifier);

	static bool IsRequiredSecretKey(const HttpHeaderIdentifiers& iden);

	void AppendIdentificationBlock(HttpHeader* pHeader) const;

protected:

	Identifier();
	virtual ~Identifier();

	void SerializeData(const VectorStringPair& data, std::string& serialized) const;

	StringPair GetTimestamp() const;
	StringPair GetClassroom() const;
	StringPair GetIPAddress(const NetProtocolIdentifiers* protocol = NULL) const;

	virtual void CreateIdentificationDataBaseBlock(VectorStringPair& data) const;
	virtual void CreateIdentificationData(VectorStringPair& data) const = 0;
	virtual void SignIdentificationData(VectorStringPair& data, std::string& sign) const = 0;

	int _iSocketFD;

private:

};
