#include "http/identification/Identifier.h"
#include "http/identification/HMACIdentifier.h"
#include "framework/Utils.h"

#include <time.h>

#ifdef UNIX
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

Identifier* Identifier::CreateIdentifier(const int& fd)
{
	Identifier* instance = NULL;
	int32 iIdentificator = CONFIG->GetIntValue(HTTP_IDENTIFICATOR);
	if (iIdentificator < HttpHeaderIdentifiers::HttpIdentifierNone || iIdentificator >= HttpHeaderIdentifiers::HttpIdentifierMax)
		iIdentificator = HttpHeaderIdentifiers::HttpIdentifierNone;

	switch ((HttpHeaderIdentifiers)iIdentificator)
	{
		case HttpIdentifierHMAC:
			instance = new HMACIdentifier();
			break;
		default:
			break;
	}

	if (instance != NULL)
		instance->_iSocketFD = fd;

	return instance;
}

void Identifier::DestroyIdentifier(Identifier* pIdentifier)
{
	if (pIdentifier)
		delete pIdentifier;
}

bool Identifier::IsRequiredSecretKey(const HttpHeaderIdentifiers& iden)
{
	switch (iden)
	{
		case HttpIdentifierHMAC:
			return true;
		default:
			return false;
	}

	return false;
}

Identifier::Identifier() : _iSocketFD(-1)
{
	LOG_TRACET(_iSocketFD, "Identifier::Identifier");
}

Identifier::~Identifier()
{
	LOG_TRACET(_iSocketFD, "Identifier::~Identifier");
}

void Identifier::AppendIdentificationBlock(HttpHeader* pHeader) const
{
	LOG_TRACET(_iSocketFD, "Identifier::AppendIdentificationBlock");

	try
	{
		VectorStringPair dataToSign;

		CreateIdentificationData(dataToSign);

		std::string signature;

		SignIdentificationData(dataToSign, signature);

		LOG_DEBUGFT(_iSocketFD, "Signature value: %s", signature.c_str());

		dataToSign.push_back(StringPair("Signature", signature.c_str()));

		pHeader->InsertIdentificationData(dataToSign);
	}
	catch (const CException& ex)
	{
		if (CONFIG->GetBoolValue(HTTP_IDENTIFY_REQUIRED))
			CException::Throw(ex);
	}
}

void Identifier::SerializeData(const VectorStringPair& data, std::string& serialized) const
{
	serialized.reserve(data.size() * 2 * 20);

	for (VectorStringPair::const_iterator itr = data.begin(); itr != data.end(); itr++)
	{
		serialized.append((*itr).first + (*itr).second);
	}
}

StringPair Identifier::GetTimestamp() const
{
	LOG_TRACET(_iSocketFD, "Identifier::GetTimestamp");

	time_t timeNow;
	time(&timeNow);
	std::string timeNowStr;

	if (Utils::lexical_cast<std::string, time_t>(timeNow, timeNowStr) != true)
	{
		LOG_WARNINGT(_iSocketFD, "Identifier::CreateIdentificationData - Error while converting timestamp value to string");
		CException::Throw(CException::LexicalCastFailed, "Error while converting timestamp value to string");
	}

	return StringPair("Timestamp", timeNowStr);
}

/*
	Based on http://stackoverflow.com/questions/212528/get-the-ip-address-of-the-machine
*/
StringPair Identifier::GetIPAddress(const NetProtocolIdentifiers* protocol/* = NULL*/) const
{
	LOG_TRACET(_iSocketFD, "Identifier::GetIPAddress");

	std::string address;

#ifdef UNIX
	struct ifaddrs* ifAddrStruct = NULL;
	struct ifaddrs* ifa = NULL;
	void* tmpAddrPtr = NULL;

	getifaddrs(&ifAddrStruct);

	for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next)
	{
		if (!ifa->ifa_addr)
			continue;

		if (((protocol && *protocol == NetProtocolIdentifiers::NET_PROTOCOL_IPV4) ||
			CONFIG->GetIntValue(HTTP_IDENTIFY_PROTOCOL) == NetProtocolIdentifiers::NET_PROTOCOL_IPV4)
			&& ifa->ifa_addr->sa_family == AF_INET)
		{

			tmpAddrPtr = &((struct sockaddr_in*)ifa->ifa_addr)->sin_addr;
			char addressBuffer[INET_ADDRSTRLEN];
			inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
			if (CONFIG->GetStrValue(HTTP_IDENTIFY_USED_NETWORK_INTERFACE) == ifa->ifa_name)
				address = addressBuffer;
		}
		else if (((protocol && *protocol == NetProtocolIdentifiers::NET_PROTOCOL_IPV6) ||
			CONFIG->GetIntValue(HTTP_IDENTIFY_PROTOCOL) == NetProtocolIdentifiers::NET_PROTOCOL_IPV6)
			&& ifa->ifa_addr->sa_family == AF_INET6)
		{

			tmpAddrPtr = &((struct sockaddr_in6*)ifa->ifa_addr)->sin6_addr;
			char addressBuffer[INET6_ADDRSTRLEN];
			inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
			if (CONFIG->GetStrValue(HTTP_IDENTIFY_USED_NETWORK_INTERFACE) == ifa->ifa_name)
				address = addressBuffer;
		}
	}

	if (ifAddrStruct)
		freeifaddrs(ifAddrStruct);
#endif

	LOG_DEBUGFT(_iSocketFD, "Identifier::GetIPAddress - IPAddress: %s", address.c_str());

	// TODO: Windows implementation

	return StringPair("IPAddress", address);
}

StringPair Identifier::GetClassroom() const
{
	LOG_TRACET(_iSocketFD, "Identifier::GetClassroom");

	std::string classroomStr;
	StringPair ipAddress = GetIPAddress();

	// TODO: Use correct data (configurable?)
	if (ipAddress.second == "10.0.2.15")
		classroomStr = "1";
	else
		classroomStr = "2";

	return StringPair("Classroom", classroomStr);
}

void Identifier::CreateIdentificationDataBaseBlock(VectorStringPair& data) const
{
	LOG_TRACET(_iSocketFD, "Identifier::CreateIdentificationDataBaseBlock");

	data.push_back(GetTimestamp());
	data.push_back(GetIPAddress());
	data.push_back(GetClassroom());
}