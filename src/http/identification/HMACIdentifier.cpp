#include "http/identification/HMACIdentifier.h"
#include "framework/Utils.h"
#include "DataMgr.h"

#include <openssl/hmac.h>

HMACIdentifier::HMACIdentifier()
{
	LOG_TRACET(_iSocketFD, "HMACIdentifier::HMACIdentifier");
}

HMACIdentifier::~HMACIdentifier()
{
	LOG_TRACET(_iSocketFD, "HMACIdentifier::~HMACIdentifier");
}

void HMACIdentifier::CreateIdentificationData(VectorStringPair& data) const
{
	LOG_TRACET(_iSocketFD, "HMACIdentifier::CreateIdentificationData");

	data.push_back(GetTimestamp());
	data.push_back(GetClassroom());
}

void HMACIdentifier::SignIdentificationData(VectorStringPair& data, std::string& sign) const
{
	LOG_TRACET(_iSocketFD, "HMACIdentifier::SignIdentificationData");

	std::string key = DATAMGR->GetHttpIdentificatorSecretKey();

	std::string dataSerialized;
	SerializeData(data, dataSerialized);

	const EVP_MD* hashFn = EVP_sha256();
	unsigned int resSize = EVP_MD_size(hashFn);
	byte* res = new byte[resSize];

	LOG_DEBUGFT(_iSocketFD, "Hash function: %s, hash length: %u", "sha256", resSize);

	HMAC_CTX ctx;
	HMAC_CTX_init(&ctx);

	if (HMAC_Init_ex(&ctx, key.c_str(), key.length(), hashFn, NULL) != 1)
		CException::Throw(CException::OpenSSLError, "Error while initializing HMAC context (HMAC_Init_ex)");

	if (HMAC_Update(&ctx, (unsigned char*)dataSerialized.c_str(), dataSerialized.length()) != 1)
		CException::Throw(CException::OpenSSLError, "Error while calculating HMAC (HMAC_Update)");

	if (HMAC_Final(&ctx, res, &resSize) != 1)
		CException::Throw(CException::OpenSSLError, "Error while calculating HMAC (HMAC_Final)");

	HMAC_CTX_cleanup(&ctx);

	Utils::ByteArrayToStringHex(res, resSize, sign);

	if (res)
		delete[] res;
}
