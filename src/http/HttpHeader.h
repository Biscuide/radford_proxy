#pragma once

#include "Radford.h"

typedef std::pair<std::string, std::string> StringPair;
typedef std::vector<StringPair> VectorStringPair;

class HttpHeader
{

public:

	friend class HttpClient;

	typedef enum ConnectionType
	{
		CONN_CLOSE = 0,
		CONN_KEEP_ALIVE = 1
	} ConnectionType;

	typedef enum EncodingType
	{
		ENC_NORMAL = 0,
		ENC_CHUNKED = 1
	} EncodingType;

	HttpHeader(const int* fd = NULL);
	virtual ~HttpHeader();

	virtual void Reset();

	bool Parse();
	void BuildHeader(std::string& res) const;

	const ByteVector& GetHeaderBuffer() const { return _vHeaderBuffer; }
	const size_t GetHeaderSize() const { return _uHeaderSize; }
	void HeaderBufferAppend(const byte* data, const size_t& len);
	bool GetFieldValue(const std::string& fieldName, std::string& fieldValue) const;
	void InsertIdentificationData(const VectorStringPair& data);

protected:

	void AddField(const std::string& name, const std::string& value);

	virtual void ParseFirstLine(const std::string& line) = 0;
	virtual void BuildFirstLine(std::string& res) const = 0;

	VectorStringPair _fields;
	size_t _uHeaderSize;
	ByteVector _vHeaderBuffer;
	int _iSocketFD;
};
