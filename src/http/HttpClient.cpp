#include "http/HttpClient.h"
#include "http/HttpsClient.h"
#include "http/identification/HMACIdentifier.h"
#include "framework/Utils.h"
#include "proxy/ProxyMgr.h"

#ifdef _WIN32

//#include <winsock2.h>
/*
#include <ws2tcpip.h>
#include <stdio.h>
#include <windows.h>
*/

#else

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>

#endif

std::string GetProtocolName(const ProtocolName& pn)
{
	switch (pn)
	{
	case PROTOCOL_HTTP:
		return "HTTP";
	case PROTOCOL_HTTPS:
		return "HTTPS";
	default:
		return "Unknown";
	}

	return "Unknown";
}

HttpClient::HttpClient(const int* fd/* = NULL*/) : _iSocketFD(fd ? *fd : SOCKET_UNDEF), _sHost(""), _uPort(0), _pResponse(NULL), _pRequest(NULL)
{
	LOG_TRACET(_iSocketFD, "HttpClient::HttpClient");

	_pSocketConn = SocketConnector::Create();
}

HttpClient::~HttpClient()
{
	LOG_TRACET(_iSocketFD, "HttpClient::~HttpClient");

	if (_iSocketFD != SOCKET_UNDEF)
		Close();

	if (_pRequest)
		delete _pRequest;

	if (_pResponse)
		delete _pResponse;

	SocketConnector::Destroy(_pSocketConn);
}

void HttpClient::Open(const std::string& sAddress, const uint16& uPort)
{
	LOG_TRACET(_iSocketFD, "HttpClient::Open");

	if (_pSocketConn == NULL)
	{
		LOG_ERROR("HttpClient::Open - Socket connector == NULL");
		CException::Throw(CException::InternalError, "Socket connector == NULL");
	}

	_iSocketFD = _pSocketConn->Connect(sAddress, uPort);

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::Open - Socket is not defined");
		CException::Throw(CException::SocketError, "Socket is not defined");
	}

	_sHost = sAddress;
	_uPort = uPort;
}

void HttpClient::Close()
{
	LOG_TRACET(_iSocketFD, "HttpClient::Close");

	if (_iSocketFD != SOCKET_UNDEF)
		_pSocketConn->Close(_iSocketFD);

	_iSocketFD = SOCKET_UNDEF;
}

uint16 HttpClient::ResendRequest(HttpClient* pServer, const uint16& uPort, HttpResponse* proxyResponse)
{
	LOG_TRACET(_iSocketFD, "HttpClient::ResendRequest");

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::ResendRequest - Socket is not defined");
		CException::Throw(CException::SocketError, "Socket is not defined");
	}

	if (_pRequest)
		delete _pRequest;

	_pRequest = new HttpRequest(&_iSocketFD);

	byte aBuffer[BUFFER_SIZE + 1];
	int iRecvBytes = 0;
	uint32 uMessageBodySize = 0;
	HttpHeader::EncodingType encoding = HttpHeader::ENC_NORMAL;
	HttpHeader::ConnectionType connection = HttpHeader::CONN_KEEP_ALIVE;
	bool bHeaderComplete = false;
	uint16 status = RS_UNK;
	uint32 uTimeoutCounter = 0;
	const uint32 uTimeoutTotal = CONFIG->GetIntValue(PROXY_TIMEOUT_CLIENT);

	struct timeval timeoutRecv;
	timeoutRecv.tv_sec = 0;
	timeoutRecv.tv_usec = 100 * 1000;

	if (setsockopt(_iSocketFD, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeoutRecv, sizeof(timeoutRecv)) < 0)
		LOG_WARNINGF("HttpClient::ResendRequest - Error while setting timeout. Socket: %i", _iSocketFD);

	struct timeval timeout;
	timeout.tv_sec = CONNECTION_CHECK_PERIOD;
	timeout.tv_usec = 0;
	fd_set sockets;

	while (status == RS_UNK)
	{
		FD_ZERO(&sockets);
		FD_SET(_iSocketFD, &sockets);

		int resSelect = select(_iSocketFD + 1, &sockets, NULL, NULL, &timeout);

		if (resSelect < 0)
		{
			LOG_ERRORT(_iSocketFD, "HttpClient::ResendRequest - Error in select");
			status = RS_ERROR;
			break;
		}

		int resFD = FD_ISSET(_iSocketFD, &sockets);

		LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - resSelect: %i, resFD: %i", resSelect, resFD);

		if (resSelect == 0 || resFD == 0)
		{
			if (++uTimeoutCounter >= uTimeoutTotal)
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendRequest - Connection timeout");
				status = RS_ERROR;
				break;
			}

			if (PROXYMGR->IsClosing())
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendRequest - Proxy is shuting down, connection closing");
				status = RS_ERROR;
				break;
			}

			timeout.tv_sec = CONNECTION_CHECK_PERIOD;
			timeout.tv_usec = 0;

			LOG_DEBUGT(_iSocketFD, "HttpClient::ResendRequest - Waiting ...");
			continue;
		}

		while (true)
		{
			iRecvBytes = Read(aBuffer, BUFFER_SIZE);

			LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Data length: %i.", iRecvBytes);

			if (iRecvBytes < 0)
			{
#ifdef WIN32
				if (WSAGetLastError() != WSAETIMEDOUT)
#else
				if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
#endif
				{
					LOG_ERRORT(_iSocketFD, "HttpClient::ResendRequest - Error while reading from socket");
					status = RS_ERROR;
				}

				break;
			}

			if (iRecvBytes == 0)
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendRequest - Close connection ...");
				status = RS_CLOSED;
				break;
			}

			//aBuffer[iRecvBytes] = '\0';

			//LOG_DEBUG_DATA((char*)aBuffer);

			if (bHeaderComplete == false)
			{
				//LOG_DEBUG_DATA((char*)aBuffer);

				_pRequest->HeaderBufferAppend(aBuffer, iRecvBytes);

				if ((bHeaderComplete = _pRequest->Parse()) == true)
				{
					std::string sMessageBodySize;
					std::string sEncoding;
					std::string sConnection;

					if (HttpRequest::HasDisabledHost(_pRequest))
					{
						/*
						GenerateResponse(RESP_CODE_BAD_REQUEST, "Host is not enabled", proxyResponse);
						status = RS_PROXY_RESP | RS_CLOSE;
						*/
						status = RS_ERROR;
						break;
					}

					if (_pRequest->GetFieldValue("Content-Length", sMessageBodySize) == true)
					{
						if (Utils::lexical_cast<uint32>(sMessageBodySize, uMessageBodySize) == false)
							uMessageBodySize = 0;
					}
					else if (_pRequest->GetFieldValue("Transfer-Encoding", sEncoding) == true && sEncoding == "chunked")
						encoding = HttpHeader::ENC_CHUNKED;

					if ((_pRequest->GetFieldValue("Connection", sConnection) == true && sConnection == "close") || _pRequest->GetRequestLine()[HttpRequest::REQ_LINE_VERSION] != "HTTP/1.1")
						connection = HttpHeader::CONN_CLOSE;

					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Connection: \"%s\", Encoding: \"%s\"", sConnection.c_str(), sEncoding.c_str());

					if (connection == HttpHeader::CONN_CLOSE)
						status = RS_CLOSE;
					else
						status = RS_KEEP_ALIVE;

					if (_pRequest->GetMethod() == HttpRequest::M_CONNECT && CONFIG->GetBoolValue(PROXY_TUNNELING_ENABLE) == false)
					{
						GenerateResponse(RESP_CODE_BAD_REQUEST, "Tunneling is not enabled", proxyResponse);
						status = RS_PROXY_RESP | RS_CLOSE;
						break;
					}
					else if (_pRequest->GetMethod() == HttpRequest::M_OPTIONS || _pRequest->GetMethod() == HttpRequest::M_TRACE)
					{
						GenerateResponse(RESP_CODE_SERVER_NYI, "Request method is not supported", proxyResponse);
						status = RS_PROXY_RESP | RS_CLOSE;
						break;
					}

					try
					{
						uint16 port = 0;
						std::string sHost;

						ParseURI(_pRequest->GetHost(), sHost, port);

						if (CONFIG->GetBoolValue(PROXY_RESPECT_REQUEST_PORT) == false || port == 0)
							port = uPort;

						// Open connection to server
						if (pServer->IsClosed() == true)
							pServer->Open(sHost, port);
						else if (sHost != pServer->GetHost() || port != pServer->GetPort())
						{
							pServer->Close();
							pServer->Open(sHost, port);
						}

					}
					catch (const CException& ex)
					{
						if (_pRequest->GetMethod() == HttpRequest::M_CONNECT)
						{
							GenerateResponse(RESP_CODE_SERVER_BAD_GATEWAY, "Cannot connect to the server", proxyResponse);
							status = RS_PROXY_RESP | RS_CLOSE;
							break;
						}

						CException::Throw(ex);
					}

					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Connection pair [Client: %i, Server: %i]", _iSocketFD, pServer->GetSocketFD());

					if (_pRequest->GetMethod() == HttpRequest::M_CONNECT)
					{
						GenerateResponse(RESP_CODE_SUCCES_OK, "Connection established", proxyResponse);
						status = RS_PROXY_RESP | RS_TUNNELING;
						break;
					}

					if (Identifier* pSigner = Identifier::CreateIdentifier(_iSocketFD))
					{
						pSigner->AppendIdentificationBlock(_pRequest);
						Identifier::DestroyIdentifier(pSigner);
					}

					// Send header
					pServer->SendRequestHeader(_pRequest);

					// Some requests does not contain message-body
					if (HttpRequest::HasMessageBody(_pRequest) == false)
					{
						if (_pRequest->GetHeaderBuffer().size() > 0)
							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Message-body is not required but buffer is not empty! Buffer size: %lu", _pRequest->GetHeaderBuffer().size());

						// Check connection keep-alive?
						break;
					}

					// Request has no body
					if (encoding == HttpHeader::ENC_NORMAL && uMessageBodySize == 0)
					{
						LOG_DEBUGT(_iSocketFD, "HttpClient::ResendRequest - Message has no body (normal encoding)");
						break;
					}

					if (_pRequest->GetHeaderBuffer().size() > 0)
					{
						const byte* pCurr = _pRequest->GetHeaderBuffer().data();
						size_t dataLeftSize = _pRequest->GetHeaderBuffer().size();

						LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Left data size: %lu", dataLeftSize);

						// Chunk response
						if (encoding == HttpHeader::ENC_CHUNKED)
						{
							pServer->SendMessageBodyChunked(pCurr, dataLeftSize, uMessageBodySize);

							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

							if (uMessageBodySize == 0 && connection == HttpHeader::CONN_CLOSE)
							{
								status = RS_CLOSE;
								break;
							}
						}
						// Normal response (with Content-Length field)
						else
						{
							LOG_DEBUGF("HttpClient::ResendRequest - Content-Length: %i", uMessageBodySize);

							pServer->SendMessageBodyNormal(pCurr, dataLeftSize, uMessageBodySize);

							LOG_DEBUG_DATA((char*)pCurr);
						
							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

							if (uMessageBodySize == 0/* && connection == HttpHeader::CONN_CLOSE*/)
							{
								status = RS_CLOSE;
								break;
							}
						}
					}
				}
			}
			else
			{
				// Chunk response
				if (encoding == HttpHeader::ENC_CHUNKED)
				{
					const byte* pCurr = aBuffer;
					size_t dataLeftSize = iRecvBytes;

					// Send previous chunk
					if (uMessageBodySize > 0)
					{
						LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Chunk size previous: %i", uMessageBodySize);

						size_t toSend = 0;

						if (uMessageBodySize > (uint32)iRecvBytes)
						{
							LOG_DEBUGT(_iSocketFD, "HttpClient::ResendRequest - Send whole buffer");

							toSend = iRecvBytes;
							uMessageBodySize -= iRecvBytes;
							dataLeftSize = 0;
						}
						else
						{
							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Send %u bytes from buffer", uMessageBodySize);

							toSend = uMessageBodySize;
							dataLeftSize -= uMessageBodySize;
							uMessageBodySize = 0;
						}

						// Send message-body (chunk)
						pServer->SendData(pCurr, toSend);

						pCurr += toSend;
					}

					if (dataLeftSize > 0)
					{
						pServer->SendMessageBodyChunked(pCurr, dataLeftSize, uMessageBodySize);

						LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

						if (uMessageBodySize == 0 && connection == HttpHeader::CONN_CLOSE)
						{
							status = RS_CLOSE;
							break;
						}
					}

					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - Chunk size (left): %i", uMessageBodySize);

				}
				// Normal response (with Content-Length field)
				else
				{
					pServer->SendMessageBodyNormal(aBuffer, (size_t)iRecvBytes, uMessageBodySize);

					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendRequest - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

					if (uMessageBodySize == 0/* && connection == HttpHeader::CONN_CLOSE*/)
					{
						LOG_INFOT(_iSocketFD, "Close connection N2 ...");
						status = RS_CLOSE;
						break;
					}
				}
			}
		}
	}

	return status;
}

bool HttpClient::ResendData(HttpClient* pDest)
{
	try
	{
		LOG_TRACET(_iSocketFD, "HttpClient::ResendData");

		if (_iSocketFD == SOCKET_UNDEF)
		{
			LOG_WARNING("HttpClient::ResendData - Socket is not defined");
			return false;
		}

		if (_pRequest)
			delete _pRequest;

		_pRequest = new HttpRequest(&_iSocketFD);

		byte aBuffer[BUFFER_SIZE + 1];
		int iRecvBytes = 0;
		uint32 uTimeoutCounter = 0;
		const uint32 uTimeoutTotal = CONFIG->GetIntValue(PROXY_TIMEOUT_CLIENT);

		struct timeval timeout;
		timeout.tv_sec = CONNECTION_CHECK_PERIOD;
		timeout.tv_usec = 0;
		fd_set sockets;

		FD_ZERO(&sockets);
		FD_SET(_iSocketFD, &sockets);

		while (!IsClosed() && !pDest->IsClosed())
		{
			if (select(_iSocketFD + 1, &sockets, NULL, NULL, &timeout) < 0)
			{
				LOG_ERRORT(_iSocketFD, "HttpClient::ResendData - Error in select");
				Close();
				return false;
			}

			if (!FD_ISSET(_iSocketFD, &sockets))
			{
				if (++uTimeoutCounter >= uTimeoutTotal)
				{
					LOG_DEBUGT(_iSocketFD, "HttpClient::ResendData - Connection timeout");
					Close();
					return false;
				}

				if (PROXYMGR->IsClosing())
				{
					LOG_DEBUGT(_iSocketFD, "HttpClient::ResendData - Proxy is shuting down, connection closing");
					Close();
					return false;
				}

				timeout.tv_sec = CONNECTION_CHECK_PERIOD;
				timeout.tv_usec = 0;

				FD_ZERO(&sockets);
				FD_SET(_iSocketFD, &sockets);
				continue;
			}

			iRecvBytes = Read(aBuffer, BUFFER_SIZE);

			if (iRecvBytes < 0)
			{
				LOG_ERRORT(_iSocketFD, "HttpClient::ResendData - Error while reading from socket");
				Close();
				return false;
			}

			if (iRecvBytes == 0)
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendData - Close connection ...");
				Close();
				return false;
			}

			pDest->SendData(aBuffer, iRecvBytes);
		}
	}
	catch (const CException& ex)
	{
		PrintEx(ex);
		return false;
	}

	return true;
}

void HttpClient::SendRequestHeader(const HttpRequest* pRequest)
{
	LOG_TRACET(_iSocketFD, "HttpClient::SendRequestHeader");

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::SendRequestHeader - Socket is not defined");
		CException::Throw(CException::SocketError, "Socket is not defined");
	}

	if (pRequest == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::SendRequestHeader - pRequest == NULL");
		CException::Throw(CException::InternalError, "pRequest == NULL");
	}

	std::string header;
	pRequest->BuildHeader(header);

	if (!header.empty())
	{
		LOG_DEBUG_DATA(header.c_str());

		SendData((const byte*)header.c_str(), header.length());
	}
}

void HttpClient::SendResponseHeader(const HttpResponse* pResponse)
{
	LOG_TRACET(_iSocketFD, "HttpClient::SendResponseHeader");

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::SendResponseHeader - Socket is not defined");
		CException::Throw(CException::SocketError, "Socket is not defined");
	}

	if (pResponse == NULL)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::SendResponseHeader - pResponse == NULL");
		CException::Throw(CException::InternalError, "pResponse == NULL");
	}

	std::string header;
	pResponse->BuildHeader(header);

	if (!header.empty())
	{
		//LOG_DEBUG_DATA(header.c_str());

		SendData((const byte*)header.c_str(), header.length());
	}
}

uint16 HttpClient::ResendResponse(HttpClient* pClient)
{
	LOG_TRACET(_iSocketFD, "HttpClient::ResendResponse");

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERRORT(_iSocketFD, "HttpClient::ResendResponse - Socket is not defined");
		CException::Throw(CException::SocketError, "Socket is not defined");
	}

	if (_pResponse)
		delete _pResponse;

	_pResponse = new HttpResponse(&_iSocketFD);

	byte aBuffer[BUFFER_SIZE + 1];
	int iRecvBytes = 0;
	uint32 uMessageBodySize = 0;
	HttpHeader::EncodingType encoding = HttpHeader::ENC_NORMAL;
	HttpHeader::ConnectionType connection = HttpHeader::CONN_KEEP_ALIVE;
	bool bHeaderComplete = false;
	uint16 status = RS_UNK;
	uint32 uTimeoutCounter = 0;
	const uint32 uTimeoutTotal = CONFIG->GetIntValue(PROXY_TIMEOUT_SERVER);

	struct timeval timeoutRecv;
	timeoutRecv.tv_sec = 0;
	timeoutRecv.tv_usec = 100 * 1000;

	if (setsockopt(_iSocketFD, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeoutRecv, sizeof(timeoutRecv)) < 0)
		LOG_WARNINGF("HttpClient::ResendRequest - Error while setting timeout. Socket: %i", _iSocketFD);

	struct timeval timeout;
	timeout.tv_sec = CONNECTION_CHECK_PERIOD;
	timeout.tv_usec = 0;
	fd_set sockets;

	while (status == RS_UNK)
	{
		FD_ZERO(&sockets);
		FD_SET(_iSocketFD, &sockets);

		int resSelect = select(_iSocketFD + 1, &sockets, NULL, NULL, &timeout);

		if (resSelect < 0)
		{
			LOG_ERRORT(_iSocketFD, "HttpClient::ResendResponse - Error in socket select");
			status = RS_ERROR;
			break;
		}

		int resFD = FD_ISSET(_iSocketFD, &sockets);

		LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - resSelect: %i, resFD: %i", resSelect, resFD);

		if (resSelect == 0 || resFD == 0)
		{
			if (++uTimeoutCounter >= uTimeoutTotal)
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendResponse - Connection timeout");
				status = RS_ERROR;
				break;
			}

			if (PROXYMGR->IsClosing())
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendResponse - Proxy is shuting down, connection closing");
				status = RS_ERROR;
				break;
			}

			LOG_DEBUGT(_iSocketFD, "HttpClient::ResendResponse - Waiting ...");
			continue;
		}

		while (true)
		{
			iRecvBytes = Read(aBuffer, BUFFER_SIZE);

			LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Data length: %i.", iRecvBytes);

			if (iRecvBytes < 0)
			{
#ifdef WIN32
				if (WSAGetLastError() != WSAETIMEDOUT)
#else
				if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
#endif
				{
					LOG_ERRORT(_iSocketFD, "HttpClient::ResendResponse - Error while reading from socket");
					status = RS_ERROR;
				}

				break;
			}

			if (iRecvBytes == 0)
			{
				LOG_DEBUGT(_iSocketFD, "HttpClient::ResendResponse - Close connection ...");
				status = RS_CLOSED;
				break;
			}

			//aBuffer[iRecvBytes] = '\0';

			//LOG_DEBUG_DATA((char*)aBuffer);

			if (bHeaderComplete == false)
			{
				//LOG_DEBUG_DATA((char*)aBuffer);

				_pResponse->HeaderBufferAppend(aBuffer, iRecvBytes);

				if ((bHeaderComplete = _pResponse->Parse()) == true)
				{
					std::string sMessageBodySize;
					std::string sEncoding;
					std::string sConnection;

					if (_pResponse->GetFieldValue("Content-Length", sMessageBodySize) == true)
					{
						if (Utils::lexical_cast<uint32>(sMessageBodySize, uMessageBodySize) == false)
							uMessageBodySize = 0;
					}
					else if (_pResponse->GetFieldValue("Transfer-Encoding", sEncoding) == true && sEncoding == "chunked")
						encoding = HttpHeader::ENC_CHUNKED;

					if ((_pResponse->GetFieldValue("Connection", sConnection) == true && sConnection == "close") || _pResponse->GetStatusLine()[HttpResponse::RESP_LINE_VERSION] != "HTTP/1.1")
						connection = HttpHeader::CONN_CLOSE;

					if (connection == HttpHeader::CONN_CLOSE)
						status = RS_CLOSE;
					else
						status = RS_KEEP_ALIVE;

					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Connection: \"%s\", Encoding: \"%s\"", sConnection.c_str(), sEncoding.c_str());

					// Send header
					pClient->SendResponseHeader(_pResponse);

					// Response has no body
					if (encoding == HttpHeader::ENC_NORMAL && uMessageBodySize == 0)
					{
						status = RS_CLOSE;
						break;
					}

					if (_pResponse->GetHeaderBuffer().size() > 0)
					{
						const byte* pCurr = _pResponse->GetHeaderBuffer().data();
						size_t dataLeftSize = _pResponse->GetHeaderBuffer().size();

						LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Left data size: %lu", dataLeftSize);

						// Chunk response
						if (encoding == HttpHeader::ENC_CHUNKED)
						{
							pClient->SendMessageBodyChunked(pCurr, dataLeftSize, uMessageBodySize);

							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

							if (uMessageBodySize == 0 && connection == HttpHeader::CONN_CLOSE)
							{
								status = RS_CLOSE;
								break;
							}

						}
						// Normal response (with Content-Length field)
						else
						{
							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Content-Length: %i", uMessageBodySize);

							pClient->SendMessageBodyNormal(pCurr, dataLeftSize, uMessageBodySize);

							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

							if (uMessageBodySize == 0/* && connection == HttpHeader::CONN_CLOSE*/)
							{
								status = RS_CLOSE;
								break;
							}
						}
					}
				}
			}
			else
			{
				// Chunk response
				if (encoding == HttpHeader::ENC_CHUNKED)
				{
					const byte* pCurr = aBuffer;
					size_t dataLeftSize = iRecvBytes;

					// Send previous chunk
					if (uMessageBodySize > 0)
					{
						LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Chunk size previous: %i", uMessageBodySize);

						size_t toSend = 0;

						if (uMessageBodySize > (uint32)iRecvBytes)
						{
							LOG_DEBUGT(_iSocketFD, "HttpClient::ResendResponse - Send whole buffer");

							toSend = iRecvBytes;
							uMessageBodySize -= iRecvBytes;
							dataLeftSize = 0;
						}
						else
						{
							LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Send %u bytes from buffer", uMessageBodySize);

							toSend = uMessageBodySize;
							dataLeftSize -= uMessageBodySize;
							uMessageBodySize = 0;
						}

						// Send message-body (chunk)
						pClient->SendData(pCurr, toSend);

						pCurr += toSend;
					}

					if (dataLeftSize > 0)
					{
						pClient->SendMessageBodyChunked(pCurr, dataLeftSize, uMessageBodySize);

						LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

						if (uMessageBodySize == 0 && connection == HttpHeader::CONN_CLOSE)
						{
							status = RS_CLOSE;
							break;
						}
					}

					// uMessageBodySize can be zero?!
					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - Chunk size (left): %i", uMessageBodySize);
				}
				// Normal response (with Content-Length field)
				else
				{
					pClient->SendMessageBodyNormal(aBuffer, (size_t)iRecvBytes, uMessageBodySize);

					LOG_DEBUGFT(_iSocketFD, "HttpClient::ResendResponse - uMessageBodySize: %u, connection: %u", uMessageBodySize, (bool)(connection == HttpHeader::CONN_CLOSE));

					if (uMessageBodySize == 0/* && connection == HttpHeader::CONN_CLOSE*/)
					{
						status = RS_CLOSE;
						break;
					}
				}
			}
		}
	}

	return status;
}

bool HttpClient::SendMessageBodyChunked(const byte* buffer, size_t dataLeftSize, uint32& uMessageBodySize)
{
	LOG_TRACET(_iSocketFD, "HttpClient::SendMessageBodyChunked");

	//LOG_DEBUG_DATA((char*)buffer);

	LoadChunkSize(buffer, dataLeftSize, uMessageBodySize);

	LOG_DEBUGFT(_iSocketFD, "HttpClient::SendMessageBodyChunked - New chunk size: %u", uMessageBodySize);

	// Process all chunks
	while (uMessageBodySize > 0 && uMessageBodySize < dataLeftSize)
	{
		// Send message-body (chunk)
		SendData(buffer, uMessageBodySize);

		buffer += uMessageBodySize;
		dataLeftSize -= uMessageBodySize;
		LoadChunkSize(buffer, dataLeftSize, uMessageBodySize);

		LOG_DEBUGFT(_iSocketFD, "HttpClient::SendMessageBodyChunked - New chunk size: %u", uMessageBodySize);
	}

	LOG_DEBUGFT(_iSocketFD, "HttpClient::SendMessageBodyChunked - After chunk process : %u dataleft: %lu", uMessageBodySize, dataLeftSize);

	// Send remaining data
	if (dataLeftSize > 0)
	{
		SendData(buffer, dataLeftSize);
		if (uMessageBodySize <= dataLeftSize)
		{
			if (uMessageBodySize < dataLeftSize)
				LOG_ERRORT(_iSocketFD, "HttpClient::SendMessageBodyChunked - Required chunk size is less than incoming data size!");

			uMessageBodySize = 0;
		}
		else
			uMessageBodySize -= dataLeftSize;
	}

	LOG_DEBUGFT(_iSocketFD, "HttpClient::SendMessageBodyChunked - Chunk size (left): %u", uMessageBodySize);

	return true;
}

bool HttpClient::SendMessageBodyNormal(const byte* buffer, size_t dataLeftSize, uint32& uMessageBodySize)
{
	LOG_TRACET(_iSocketFD, "HttpClient::SendMessageBodyNormal");

	LOG_DEBUGFT(_iSocketFD, "HttpClient::SendMessageBodyNormal - Content-Length: %u, dataLeftSize: %lu", uMessageBodySize, dataLeftSize);

	if (uMessageBodySize > 0 && uMessageBodySize < dataLeftSize)
		LOG_ERRORT(_iSocketFD, "HttpClient::SendMessageBodyNormal - Required data size is less than incoming data size!");

	// Send message-body
	if (dataLeftSize > 0)
	{
		SendData(buffer, dataLeftSize);

		if (uMessageBodySize <= dataLeftSize)
		{
			uMessageBodySize = 0;
		}
		else
			uMessageBodySize -= dataLeftSize;
	}

	return true;
}

void HttpClient::GenerateResponse(const uint16& statusCode, const std::string& reason, HttpResponse* res)
{
	LOG_TRACET(_iSocketFD, "HttpClient::GenerateResponse");

	res->Reset();

	res->SetResponseLine(HTTPv1_1_PROTOCOL, statusCode, reason);

	res->AddField("Content-Length", "0");
	//res->AddField("Connection", "Closed");
}

int HttpClient::Read(byte* buffer, const size_t& bufferSize)
{
#ifdef _WIN32
	return recv(_iSocketFD, (char*)buffer, bufferSize, 0);
#else
	return recv(_iSocketFD, buffer, bufferSize, 0);
#endif
}

int HttpClient::Write(const byte* buffer, const size_t& bufferSize)
{
#ifdef _WIN32
	return send(_iSocketFD, (char*)buffer, bufferSize, 0);
#else
	return send(_iSocketFD, buffer, bufferSize, 0);
#endif
}

void HttpClient::SendData(const byte* data, const size_t& len)
{
	LOG_TRACET(_iSocketFD, "HttpClient::SendData");

	LOG_DEBUGFT(_iSocketFD, "HttpClient::SendData - Len: %lu", len);

	if (_iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERROR("HttpClient::SendData - Socket is not defined");
		CException::Throw(CException::SocketError, "Socket is not defined");
	}

	int iWriteBytes = 0;
	size_t iWriteBytesTotal = 0;

	while (iWriteBytesTotal < len)
	{
		iWriteBytes = Write(data + iWriteBytesTotal, len - iWriteBytesTotal);

		if (iWriteBytes < 0)
		{
			LOG_ERRORT(_iSocketFD, "HttpClient::SendData - Error while writing into socket");
			CException::Throw(CException::SocketError, "Error while writing into socket");
		}

		iWriteBytesTotal += iWriteBytes;
	}
}

bool HttpClient::LoadChunkSize(const byte* data, const size_t& dataSize, uint32& uMessageBodySize)
{
	LOG_TRACET(_iSocketFD, "HttpClient::LoadChunkSize");

	size_t uChunkNumberLen = 0;
	// Get length of hexadecimal number
	while (uChunkNumberLen < dataSize && *(data + uChunkNumberLen) != ASCII_CR && isxdigit(*(data + uChunkNumberLen)) != 0)
	{
		uChunkNumberLen++;
	}

	LOG_DEBUGFT(_iSocketFD, "HttpClient::LoadChunkSize - uChunkNumberLen: %lu", uChunkNumberLen);

	std::string sMessageBodySize = std::string((const char*)data, uChunkNumberLen);
	LOG_DEBUGFT(_iSocketFD, "HttpClient::LoadChunkSize - sMessageBodySize: %s", sMessageBodySize.c_str());
	
	bool res = Utils::HexToUint(sMessageBodySize, uMessageBodySize);

	LOG_DEBUGFT(_iSocketFD, "HttpClient::LoadChunkSize - iMessageBodySize: %u", uMessageBodySize);
	
	if (res == true)
		uMessageBodySize += uChunkNumberLen + CHUNK_BORDER_SIZE;
	else
		uMessageBodySize = 0;

	return res;
}

/*
Source: http://stackoverflow.com/questions/2616011/easy-way-to-parse-a-url-in-c-cross-platform
*/
void HttpClient::ParseURI(const std::string& uri, std::string& host, uint16& uPort)
{
	LOG_TRACET(_iSocketFD, "HttpClient::ParseURI");

	typedef std::string::const_iterator strItr;

	if (uri.length() == 0)
		return;

	strItr uriEnd = uri.end();

	// get query start
	strItr queryStart = std::find(uri.begin(), uriEnd, '?');

	// protocol
	strItr protocolStart = uri.begin();
	strItr protocolEnd = std::find(protocolStart, uriEnd, ':');            //"://");

	if (protocolEnd != uriEnd)
	{
		std::string prot = &*(protocolEnd);
		if ((prot.length() > 3) && (prot.substr(0, 3) == "://"))
			protocolEnd += 3;   //      ://
		else
			protocolEnd = uri.begin();  // no protocol
	}
	else
		protocolEnd = uri.begin();  // no protocol

									// host
	strItr hostStart = protocolEnd;
	strItr pathStart = std::find(hostStart, uriEnd, '/');  // get pathStart

	strItr hostEnd = std::find(protocolEnd, (pathStart != uriEnd) ? pathStart : queryStart, ':');  // check for port

	host = std::string(hostStart, hostEnd);

	std::string sPort;

	// port
	if ((hostEnd != uriEnd) && ((&*(hostEnd))[0] == ':'))  // we have a port
	{
		hostEnd++;
		strItr portEnd = (pathStart != uriEnd) ? pathStart : queryStart;
		sPort = std::string(hostEnd, portEnd);
	}

	if (!sPort.empty())
	{
		if (Utils::lexical_cast<uint16>(sPort, uPort) == false)
			uPort = 0;
	}
}
