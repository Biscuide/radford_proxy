#pragma once

#include "http/HttpHeader.h"
#include "Radford.h"

#include <stddef.h>

class HttpRequest : public HttpHeader
{

public:

	static bool HasMessageBody(const HttpRequest* pReq);

    typedef enum HttpReqLine
    {
        REQ_LINE_METHOD,
        REQ_LINE_URI,
        REQ_LINE_VERSION,
        REQ_LINE_MAX
    } HttpReqLine;

    typedef enum HttpReqMethod
    {
        REQ_METHOD_UNK,
        M_GET,
        M_HEAD,
        M_POST,
        M_PUT,
        M_DELETE,
        M_CONNECT,
        M_OPTIONS,
        M_TRACE,
        M_MAX
    } HttpReqMethod;

	HttpRequest(const int* fd = NULL);
	virtual ~HttpRequest();

    void Reset();

    const HttpReqMethod GetMethod() const { return _method; }
	const std::string GetURL() const;
    const std::string GetHost() const;
	const std::string* GetRequestLine() const { return _requestLine; }

	static bool HasDisabledHost(const HttpRequest* pReq);

protected:

	void SetRequestLine(const std::string& method, const std::string& uri, const std::string& protocol);

private:

	void ParseFirstLine(const std::string& line);
	void BuildFirstLine(std::string& res) const;

    void ParseMethod(const std::string& str);

    HttpReqMethod _method;
	std::string _requestLine[REQ_LINE_MAX];

	static std::vector<std::string> _enabledHosts;

};
