#include "CException.h"

using namespace std;

void CException::Throw(const uint16& id, const std::string& description/* = ""*/)
{
	throw CException(id, description);
}

void CException::Throw(const CException& ex)
{
	throw ex;
}

CException::CException(const uint16& id/* = NoError*/, const std::string& description/* = ""*/)
{
	SetCode(id);
	_description = description;
}

string CException::GetErrorCodeMsg(const uint16& id)
{
	string msg = "";

	switch (id)
	{
	case NoError:
		msg = "No Error.";
		break;
	case InternalError:
		msg = "Internal error.";
		break;
	case ConfigError:
		msg = "Config error.";
		break;
	case SocketError:
		msg = "Socket error.";
		break;
	case WinSockError:
		msg = "Windows socket error.";
		break;
	case NetworkError:
		msg = "Network error.";
		break;
	case OpenSSLError:
		msg = "OpenSSL error.";
		break;
	case LexicalCastFailed:
		msg = "Utils lexical cast failed.";
		break;
	case CertificateVerifyError:
		msg = "Error while certificate verification process.";
		break;
	case ParamError:
		msg = "Input parameter invalid value.";
		break;
	case InputDataError:
		msg = "Error while processing input data.";
		break;
	default:
		msg = "Unknown error.";
	}

	return msg;
}

void CException::SetCode(const uint16& id)
{
	_code = id;
	_msg = GetErrorCodeMsg(id);
}
