#pragma once

#include "Defines.h"

class CException
{

public:

	typedef enum Ids
	{
		NoError = 0,
		InternalError,
		ConfigError,
		SocketError,
		WinSockError,
		NetworkError,
		OpenSSLError,
		LexicalCastFailed,
		CertificateVerifyError,
		ParamError,
		InputDataError,

		MAX_EXCEPTION_ID
	} Ids;

	CException(const uint16& id = NoError, const std::string& description = "");

	uint16 GetId() const { return _code; }
	const std::string& GetMsg() const { return _msg; }
	const std::string& GetDescription() const { return _description; }

	static void Throw(const uint16& id, const std::string& description = "");
	static void Throw(const CException& ex);

private:

	void SetCode(const uint16& id);
	static std::string GetErrorCodeMsg(const uint16& id);

	uint16 _code;
	std::string _msg;
	std::string _description;
};
