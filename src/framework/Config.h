#pragma once

#include "Defines.h"

typedef enum IntConfigItems
{
	LOG_INTERFACE = 0,
	LOG_LEVEL,
	LOG_FORMAT,
	LOG_FILENAME_FORMAT,
	LOG_FILE_MODE,

	PROXY_HTTP_PORT_INPUT,
	PROXY_HTTP_PORT_OUTPUT,
	PROXY_HTTPS_PORT_INPUT,
	PROXY_HTTPS_PORT_OUTPUT,
	PROXY_PROTOCOL_THREADS,
	PROXY_TIMEOUT_CLIENT,
	PROXY_TIMEOUT_SERVER,
	
	NET_PROTOCOL_VERSION,

	HTTP_IDENTIFICATOR,
	HTTP_IDENTIFY_PROTOCOL,

	INT_CONFIG_MAX
} IntConfigItems;

typedef enum StrConfigItems
{
	LOG_FILE_PATH = 0,

	CERT_END_CERT_PATH,
	CERT_END_CERT_PK_PATH,
	CERT_END_CERT_PK_PASS,
	CERT_EXTRA_CERTS_PATH,
	CERT_TRUST_CERTS_PATH,
	CERT_TRUST_CERT_FOLDER_PATH,

	PROXY_ENABLED_HOSTS,

	HTTP_IDENTIFY_SECRET_KEY_PATH,
	HTTP_IDENTIFY_USED_NETWORK_INTERFACE,

	STR_CONFIG_MAX
} StrConfigItems;

typedef enum BoolConfigItems
{
	PROXY_HTTP_ENABLE,
	PROXY_HTTPS_ENABLE,
	PROXY_TUNNELING_ENABLE,
	PROXY_RESPECT_REQUEST_PORT,

	SSL_USE_SYSTEM_TRUST_STORE,
	SSL_VERIFY_WHOLE_CHAIN_CUSTOM,

	HTTP_IDENTIFY_REQUIRED,

	BOOL_CONFIG_MAX
} BoolConfigItems;


class Config
{
public:

	static void Initialize();
	static void Deinitialize();

	void Load();

	inline static Config* Instance() { return _instance; }

	int32 GetIntValue(const IntConfigItems& id) const { return _intValues[id]; }
	const std::string& GetStrValue(const StrConfigItems& id) const { return _strValues[id]; }
	const bool GetBoolValue(const BoolConfigItems& id) const { return _boolValues[id]; }

private:

	Config() {}
	~Config() {}

	void SetIntValue(const IntConfigItems& id, const int32& val) { _intValues[id] = val; }
	void SetStrValue(const StrConfigItems& id, const std::string& val) { _strValues[id] = val; }
	void SetBoolValue(const BoolConfigItems& id, const bool val) { _boolValues[id] = val; }

	void LoadIntValue(const IntConfigItems& id, const char* name, int32 defaultValue = 0, int32 minValue = 0, int32 maxValue = 0);
	void LoadStrValue(const StrConfigItems& id, const char* name, const char* defaultValue = "");
	void LoadBoolValue(const BoolConfigItems& id, const char* name, bool defaultValue = false);

	static Config* _instance;

	int32 _intValues[INT_CONFIG_MAX];
	std::string _strValues[STR_CONFIG_MAX];
	bool _boolValues[BOOL_CONFIG_MAX];

	typedef std::map<std::string, std::string> ConfigItems;
	ConfigItems _items;
};

#define CONFIG Config::Instance()
