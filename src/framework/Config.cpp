#include "framework/Config.h"
#include "framework/CException.h"
#include "framework/Logger.h"
#include "framework/Utils.h"
#include "socket/SocketConnector.h"
#include "http/identification/Identifier.h"

#include <string.h>
#include <fstream>

using namespace std;

#define MAX_LINE_LEN 1024

Config* Config::_instance = NULL;

void Config::Initialize()
{
	_instance = new Config();
}

void Config::Deinitialize()
{
	if (_instance)
		delete _instance;

	_instance = NULL;
}

void Config::Load()
{
	ifstream file;

	file.open("radford.cfg", ifstream::in);

	char buffer[MAX_LINE_LEN];

	if (!file.is_open() || file.bad())
	{
		fprintf(stderr, "WARNING: Cannot open configuration file.\n");
		fprintf(stdout, "INFO: Loading default configuration ...\n");
	}
	else
	{
		fprintf(stdout, "INFO: Loading configuration file ...\n");

		while (true)
		{
			file.getline(buffer, MAX_LINE_LEN);

			if (file.eof())
				break;

			if (file.bad())
				CException::Throw(CException::ConfigError, "Error while parsing config file.");

			int len = strlen(buffer);

			if (len == 0 || buffer[0] == '#')
				continue;

			char* eqPtr = strchr(buffer, '=');
			if (eqPtr == NULL)
				CException::Throw(CException::ConfigError, "Invalid config item.");

#ifdef _WIN32
			_items[string(buffer, eqPtr - buffer)] = string(eqPtr + 1, buffer + len - eqPtr - 1);
#else
			_items[string(buffer, eqPtr - buffer)] = string(eqPtr + 1, buffer + len - eqPtr - 2);
#endif
			//		cout << buffer << endl;
			//		cout << string(buffer, eqPtr - buffer).c_str() << "=\'" << string(eqPtr + 1, buffer + len - eqPtr - 2).c_str() << "\'" << endl;
		}
	}

	LoadIntValue(LOG_INTERFACE, "Logging.Interface", Logger::InterfaceConsole, 0, Logger::InterfaceAll);
	LoadIntValue(LOG_LEVEL, "Logging.LogLevel", Logger::LevelWarning | Logger::LevelError, 0, Logger::LevelAll);
	LoadIntValue(LOG_FORMAT, "Logging.LogFormat", Logger::FormatAll, 0, Logger::FormatAll);
	LoadIntValue(LOG_FILENAME_FORMAT, "Logging.File.NameFormat", Logger::FilenameFormatTime, 0, Logger::FilenameFormatTime);
	LoadIntValue(LOG_FILE_MODE, "Logging.File.Mode", Logger::ModeAppend, 0, Logger::ModeAppend);

	LoadIntValue(PROXY_HTTP_PORT_INPUT, "Proxy.Http.InputPort", 80, 1, 65535);
	LoadIntValue(PROXY_HTTPS_PORT_INPUT, "Proxy.Https.InputPort", 443, 1, 65535);
	LoadIntValue(PROXY_HTTP_PORT_OUTPUT, "Proxy.Http.OutputPort", 80, 1, 65535);
	LoadIntValue(PROXY_HTTPS_PORT_OUTPUT, "Proxy.Https.OutputPort", 443, 1, 65535);
	LoadIntValue(PROXY_PROTOCOL_THREADS, "Proxy.Protocol.Threads", 10, 2, 25);
	LoadIntValue(PROXY_TIMEOUT_CLIENT, "Proxy.Timeout.Client", 5, 1, 30);
	LoadIntValue(PROXY_TIMEOUT_SERVER, "Proxy.Timeout.Server", 5, 1, 30);

	LoadIntValue(NET_PROTOCOL_VERSION, "Net.Protocols.Enabled", NET_IPV4, 0, NET_ALL);

	LoadIntValue(HTTP_IDENTIFICATOR, "Http.Identificator", HttpHeaderIdentifiers::HttpIdentifierNone, 0, HttpHeaderIdentifiers::HttpIdentifierMax);
	LoadIntValue(HTTP_IDENTIFY_PROTOCOL, "Http.Identificator.Protocol.Used", NetProtocolIdentifiers::NET_PROTOCOL_IPV4, 0, NetProtocolIdentifiers::NET_PROTOCOL_IPV6);
	
	LoadStrValue(LOG_FILE_PATH, "Logging.File.Path", "radford.log");

	LoadStrValue(CERT_END_CERT_PATH, "SSL.Proxy.EndCert.Path", "");
	LoadStrValue(CERT_END_CERT_PK_PATH, "SSL.Proxy.EndCert.PrivateKey.Path", "");
	LoadStrValue(CERT_END_CERT_PK_PASS, "SSL.Proxy.EndCert.PrivateKey.Password", "");
	LoadStrValue(CERT_EXTRA_CERTS_PATH, "SSL.Proxy.ExtraCerts.Path", "");
	LoadStrValue(CERT_TRUST_CERTS_PATH, "SSL.Server.TrustCerts.Path", "");
	LoadStrValue(CERT_TRUST_CERT_FOLDER_PATH, "SSL.Server.TrustStore.FolderPath", "");

	LoadStrValue(PROXY_ENABLED_HOSTS, "Proxy.EnabledHosts", "");

	LoadStrValue(HTTP_IDENTIFY_SECRET_KEY_PATH, "Http.Identificator.SecretKey.Path", "");
	LoadStrValue(HTTP_IDENTIFY_USED_NETWORK_INTERFACE, "Http.Identificator.NetworkInterface.Used", "");

	LoadBoolValue(PROXY_HTTP_ENABLE, "Proxy.Http.Protocol", true);
	LoadBoolValue(PROXY_HTTPS_ENABLE, "Proxy.Https.Protocol", true);
	LoadBoolValue(PROXY_TUNNELING_ENABLE, "Proxy.Tunneling", false);
	LoadBoolValue(PROXY_RESPECT_REQUEST_PORT, "Proxy.Request.UsePort", true);

	LoadBoolValue(SSL_USE_SYSTEM_TRUST_STORE, "SSL.Server.TrustStore.UseSystem", true);
	LoadBoolValue(SSL_VERIFY_WHOLE_CHAIN_CUSTOM, "SSL.Server.VerifyCertChainCustom", false);

	LoadBoolValue(HTTP_IDENTIFY_REQUIRED, "Http.Identify.Required", false);	
}

void Config::LoadIntValue(const IntConfigItems& id, const char* name, int32 defaultValue/* = 0*/, int32 minValue/* = 0*/, int32 maxValue/* = 0*/)
{
	ConfigItems::iterator itr = _items.find(name);
	if (itr != _items.end())
	{
		int32 val;
		if (Utils::lexical_cast<int32>(itr->second, val) == true)
		{
			if (val < minValue)
				val = minValue;
			else if (val > maxValue)
				val = maxValue;

			_intValues[id] = val;
		}
		else
			_intValues[id] = defaultValue;
	}
	else
		_intValues[id] = defaultValue;
}

void Config::LoadStrValue(const StrConfigItems& id, const char* name, const char* defaultValue/* = ""*/)
{
	ConfigItems::iterator itr = _items.find(name);
	if (itr != _items.end())
		_strValues[id] = itr->second.c_str();
	else
		_strValues[id] = defaultValue;
}

void Config::LoadBoolValue(const BoolConfigItems& id, const char* name, bool defaultValue/* = false*/)
{
	ConfigItems::iterator itr = _items.find(name);
	if (itr != _items.end())
	{
		if (itr->second == "1")
			_boolValues[id] = true;
		else
			_boolValues[id] = false;
	}
	else
		_boolValues[id] = defaultValue;
}
