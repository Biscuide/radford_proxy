#pragma once

#include "proxy/Proxy.h"

#include <string>
#include <string.h>

typedef struct LogSettings
{
	uint interfaces;
	uint logLevel;
	uint logFormat;
	std::string filePath;
} LogSettings;

class Logger
{

public:

	typedef enum LogInterfaces
	{
		InterfaceNone		= 0x00,
		InterfaceConsole	= 0x01,
		InterfaceFile		= 0x02,

		InterfaceAll		= InterfaceConsole | InterfaceFile
	} LogInterfaces;

	typedef enum LogLevel
	{
		LevelNone		= 0x00,
		LevelInfo		= 0x01,
		LevelWarning	= 0x02,
		LevelError		= 0x04,
		LevelDebug		= 0x08,
		LevelDebugData	= 0x10,
		LevelTrace		= 0x20,

		LevelAll = LevelInfo | LevelWarning | LevelError | LevelDebug | LevelDebugData | LevelTrace
	} LogLevel;

	typedef enum LogFormat
	{
		FormatBasic		= 0x00,
		FormatTime		= 0x01,
		FormatFileInfo	= 0x02,

		FormatAll		= FormatTime | FormatFileInfo
	} LogFormat;

	typedef enum LogFilenameFormat
	{
		FilenameFormatBasic = 0,
		FilenameFormatTime
	} LogFilenameFormat;

	typedef enum LogFileMode
	{
		ModeWrite = 0,
		ModeAppend
	} LogFileMode;

	static void Initialize();
	static void Deinitialize();

	static void Write(const uint& level, const char* msg);
	static void Write(const uint& level, const char* fileName, const uint& line, const int& threadID, const char* msg);
	static void WriteFormat(const uint& level, const char* fileName, const uint& line, const int& threadID, const char* format, ...);

	inline static bool IsInfo() { return (_settings.logLevel & LevelInfo) != 0; }
	inline static bool IsWarning() { return (_settings.logLevel & LevelWarning) != 0; }
	inline static bool IsError() { return (_settings.logLevel & LevelError) != 0; }
	inline static bool IsDebug() { return (_settings.logLevel & LevelDebug) != 0; }
	inline static bool IsDebugData() { return (_settings.logLevel & LevelDebugData) != 0; }
	inline static bool IsTrace() { return (_settings.logLevel & LevelTrace) != 0; }

	inline static bool IsInitialized() { return _initialized; }

private:

	Logger() {}
	~Logger() {}

	static void Write(const char* format, va_list args);
	static void Write(const char* msg, const int& len);

	inline static bool CheckLevel(const uint& level) { return (_settings.logLevel & level) != 0; }

	inline static bool CheckConsole() { return (_settings.interfaces & InterfaceConsole) != 0; }
	inline static bool CheckFile() { return (_settings.interfaces & InterfaceFile) != 0 && _logFile != NULL; }

	static int FormatMsg(char* buffer, const uint& level, const char* fileName, const uint& line, const char* format, const int& threadID);
	static const char* LevelToString(const uint& level);

	static LogSettings _settings;
	static FILE* _logFile;
	static bool _initialized;
};

#ifdef _WIN32
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define LOG_INFOT(threadID, msg) \
	Logger::Write(Logger::LevelInfo, __FILENAME__, __LINE__, threadID, msg);

#define LOG_INFOFT(threadID, format, ...) \
	Logger::WriteFormat(Logger::LevelInfo, __FILENAME__, __LINE__, threadID, format, __VA_ARGS__)

#define LOG_INFO(msg) LOG_INFOT(-1, msg)
#define LOG_INFOF(format, ...) LOG_INFOFT(-1, format, __VA_ARGS__)

#define LOG_WARNINGT(threadID, msg) \
	Logger::Write(Logger::LevelWarning, __FILENAME__, __LINE__, threadID, msg);

#define LOG_WARNINGFT(threadID, format, ...) \
	Logger::WriteFormat(Logger::LevelWarning, __FILENAME__, __LINE__, threadID, format, __VA_ARGS__)

#define LOG_WARNING(msg) LOG_WARNINGT(-1, msg)
#define LOG_WARNINGF(format, ...) LOG_WARNINGFT(-1, format, __VA_ARGS__)

#define LOG_ERRORT(threadID, msg) \
	Logger::Write(Logger::LevelError, __FILENAME__, __LINE__, threadID, msg);

#define LOG_ERRORFT(threadID, format, ...) \
	Logger::WriteFormat(Logger::LevelError, __FILENAME__, __LINE__, threadID, format, __VA_ARGS__)

#define LOG_ERROR(msg) LOG_ERRORT(-1, msg)
#define LOG_ERRORF(format, ...) LOG_ERRORFT(-1, format, __VA_ARGS__)

#define LOG_DEBUGT(threadID, msg) \
	Logger::Write(Logger::LevelDebug, __FILENAME__, __LINE__, threadID, msg)

#define LOG_DEBUGFT(threadID, format, ...) \
	Logger::WriteFormat(Logger::LevelDebug, __FILENAME__, __LINE__, threadID, format, __VA_ARGS__)

#define LOG_DEBUG(msg) LOG_DEBUGT(-1, msg)
#define LOG_DEBUGF(format, ...) LOG_DEBUGFT(-1, format, __VA_ARGS__)

#define LOG_DEBUG_DATA(msg) \
	Logger::Write(Logger::LevelDebugData, msg)

#define LOG_TRACET(threadID, msg) \
	Logger::Write(Logger::LevelTrace, __FILENAME__, __LINE__, threadID, msg)

#define LOG_TRACE(msg) LOG_TRACET(-1, msg)
