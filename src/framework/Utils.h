#pragma once

#include "Defines.h"

#include <sstream>
#include <algorithm>

class Utils
{
public:

	static void CreateFileName(const std::string& filePathFull, const std::string& appendStr, std::string& fileNameNew);
	static char* PrintFormatBuffer(char* buffer, const size_t& bufferSize, const char* format, ...);
	static bool HexToUint(const std::string& hexValue, uint32& result);
	static bool ByteArrayToStringHex(const byte* data, const size_t& dataSize, std::string& out);
	static bool LoadTextFile(const char* filename, std::string& content);

	template<class Iter, class T, class Compare>
	static Iter BinaryFind(Iter begin, Iter end, T val, Compare comp)
	{
		Iter i = std::lower_bound(begin, end, val, comp);

		if (i != end && !comp(val, *i))
			return i;

		return end;
	}

	template <typename T>
	static bool lexical_cast(const std::string& str, T& res)
	{
		std::istringstream iss;
		iss.str(str);
		iss >> res;
		return !iss.fail();
	}

	template <typename T, typename V>
	static bool lexical_cast(const V& val, T& res)
	{
		std::stringstream iss;
		iss << val;
		iss >> res;
		return !iss.fail();
	}
};
