#include "framework/Logger.h"
#include "framework/Utils.h"

#include <stdarg.h>
#include <time.h>
#include <string.h>

#define DATETIME_STR_LEN	20

using namespace std;

LogSettings Logger::_settings = LogSettings();
FILE* Logger::_logFile = NULL;
bool Logger::_initialized = false;

void Logger::Initialize()
{
	_settings.interfaces = CONFIG->GetIntValue(LOG_INTERFACE);
	_settings.logLevel = CONFIG->GetIntValue(LOG_LEVEL);
	_settings.logFormat = CONFIG->GetIntValue(LOG_FORMAT);

	if (CONFIG->GetIntValue(LOG_FILENAME_FORMAT) & FilenameFormatTime)
	{
		time_t rawtime;
		time(&rawtime);
		char buff[DATETIME_STR_LEN + 2];
#ifdef _WIN32
		struct tm timeinfo;
		localtime_s(&timeinfo, &rawtime);
		Utils::PrintFormatBuffer(buff, DATETIME_STR_LEN + 2, "_%04d-%02d-%02d_%02d-%02d-%02d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
#else
		struct tm* timeinfo;
		timeinfo = localtime(&rawtime);
		Utils::PrintFormatBuffer(buff, DATETIME_STR_LEN + 2, "_%04d-%02d-%02d_%02d-%02d-%02d", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
#endif

		Utils::CreateFileName(CONFIG->GetStrValue(LOG_FILE_PATH), buff, _settings.filePath);
	}
	else
		_settings.filePath = CONFIG->GetStrValue(LOG_FILE_PATH);

	string mode = "w";

	if (CONFIG->GetIntValue(LOG_FILE_MODE) & ModeAppend)
		mode = "a";

	if ((_settings.interfaces & InterfaceFile) != 0)
	{
		_logFile = fopen(_settings.filePath.c_str(), mode.c_str());
		if (_logFile == NULL || ferror(_logFile) != 0)
			CException::Throw(CException::InputDataError, "Cannot open or create log file");
	}

	_initialized = true;
}

void Logger::Deinitialize()
{
	if (_logFile)
	{
		fflush(_logFile);
		fclose(_logFile);
	}
}

int Logger::FormatMsg(char* buffer, const uint& level, const char* fileName, const uint& line, const char* format, const int& threadID)
{
	int len = 0;

	if ((_settings.logFormat & FormatAll) == FormatAll)
	{
		time_t rawtime;
		time(&rawtime);
#ifdef _WIN32
		struct tm timeinfo;
		localtime_s(&timeinfo, &rawtime);
		len = _snprintf_s(buffer, MSG_BUFFER_MAX_LEN, MSG_BUFFER_MAX_LEN, "[%04d-%02d-%02d %02d:%02d:%02d] [%s:%u] %s (%i) - %s\n",
			timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec,
			fileName, line, LevelToString(level), threadID, format);
#else
		struct tm* timeinfo = localtime(&rawtime);
		len = snprintf(buffer, MSG_BUFFER_MAX_LEN, "[%04d-%02d-%02d %02d:%02d:%02d] [%s:%u] %s (%i) - %s\n",
			timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,
			fileName, line, LevelToString(level), threadID, format);
#endif
	}
	else if ((_settings.logFormat & FormatTime) != 0)
	{
		time_t rawtime;
		time(&rawtime);
#ifdef _WIN32
			struct tm timeinfo;
			localtime_s(&timeinfo, &rawtime);
			len = _snprintf_s(buffer, MSG_BUFFER_MAX_LEN, MSG_BUFFER_MAX_LEN, "[%04d-%02d-%02d %02d:%02d:%02d] %s (%i) - %s\n",
				timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec,
				LevelToString(level), threadID, format);
#else
			struct tm* timeinfo = localtime(&rawtime);
			len = snprintf(buffer, MSG_BUFFER_MAX_LEN, "[%04d-%02d-%02d %02d:%02d:%02d] %s (%i) - %s\n",
				timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,
				LevelToString(level), threadID, format);
#endif
	}
	else if ((_settings.logFormat & FormatFileInfo) != 0)
	{
#ifdef _WIN32
		len = _snprintf_s(buffer, MSG_BUFFER_MAX_LEN, MSG_BUFFER_MAX_LEN, "[%s:%u] %s (%i) - %s\n", fileName, line, LevelToString(level), threadID, format);
#else
		len = snprintf(buffer, MSG_BUFFER_MAX_LEN, "[%s:%u] %s (%i) - %s\n", fileName, line, LevelToString(level), threadID, format);
#endif
	}
	else
	{
#ifdef _WIN32
		len = _snprintf_s(buffer, MSG_BUFFER_MAX_LEN, MSG_BUFFER_MAX_LEN, "%s (%i) - %s\n", LevelToString(level), threadID, format);
#else
		len = snprintf(buffer, MSG_BUFFER_MAX_LEN, "%s (%i) - %s\n", LevelToString(level), threadID, format);
#endif
	}

	if (len >= 0)
		buffer[len] = '\0';

	return len;
}

void Logger::Write(const uint& level, const char* msg)
{
	if (!CheckLevel(level))
		return;

	Write(msg, strlen(msg));
}

void Logger::Write(const uint& level, const char* fileName, const uint& line, const int& threadID, const char* msg)
{
	if (!CheckLevel(level))
		return;

	char buffer[MSG_BUFFER_MAX_LEN];
	int buffLen = FormatMsg(buffer, level, fileName, line, msg, threadID);

	if (buffLen <= 0)
		return;

	if (CheckConsole())
		fwrite(buffer, 1, buffLen, stdout);

	if (CheckFile())
		fwrite(buffer, 1, buffLen, _logFile);
}

void Logger::WriteFormat(const uint& level, const char* fileName, const uint& line, const int& threadID, const char* format, ...)
{
	if (!CheckLevel(level))
		return;

	char formatBuffer[MSG_BUFFER_MAX_LEN];
	FormatMsg(formatBuffer, level, fileName, line, format, threadID);

	va_list argPtrMsg;
	va_start(argPtrMsg, format);

	Write(formatBuffer, argPtrMsg);

	va_end(argPtrMsg);
}

void Logger::Write(const char* format, va_list args)
{
	char buffer[MSG_BUFFER_MAX_LEN];

	int len = 0;

#ifdef _WIN32
	len = vsnprintf_s(buffer, MSG_BUFFER_MAX_LEN, format, args);
#else
	len = vsnprintf(buffer, MSG_BUFFER_MAX_LEN, format, args);
#endif

	Write(buffer, len);
}

void Logger::Write(const char* msg, const int& len)
{
	if (CheckConsole())
		fwrite(msg, 1, len, stdout);

	if (CheckFile())
		fwrite(msg, 1, len, _logFile);
}

const char* Logger::LevelToString(const uint& level)
{
	switch (level)
	{
		case LevelInfo:
			return "INFO";
		case LevelWarning:
			return "WARNING";
		case LevelError:
			return "ERROR";
		case LevelDebug:
			return "DEBUG";
		case LevelTrace:
			return "TRACE";
	}

	return "UNK";
}
