
#include "Utils.h"

#include <stdarg.h>
#include <iostream>
#include <fstream>

#ifdef _WIN32
#include <Windows.h>
#endif

using namespace std;

void Utils::CreateFileName(const string& filePathFull, const string& appendStr, string& fileNameNew)
{
	string filePath;
	string fileName;
	string fileNameFull;
	string ext;

	// Sestaveni jmena souboru
#ifdef _WIN32
	size_t slashPos = filePathFull.rfind("\\");
#else
	size_t slashPos = filePathFull.rfind("/");
#endif

	if (slashPos == string::npos)
		fileNameFull = filePathFull;
	else
	{
		fileNameFull = filePathFull.substr(slashPos + 1, filePathFull.length() - slashPos);
		filePath = filePathFull.substr(0, slashPos + 1);
	}

	size_t dotPos = fileNameFull.rfind(".");
	if (dotPos == string::npos)
		dotPos = fileNameFull.length();

	if (dotPos == 0)
		fileName = fileNameFull;
	else
	{
		fileName = fileNameFull.substr(0, dotPos);
		ext = fileNameFull.substr(dotPos, fileNameFull.length() - dotPos);
	}

	const size_t fileNameNewLen = filePath.length() + fileName.length() + appendStr.length() + ext.length() + 1;
	char* fileNameTmp = new char[fileNameNewLen];

#ifdef _WIN32
	int len = sprintf_s(fileNameTmp, fileNameNewLen, "%s%s%s%s", filePath.c_str(), fileName.c_str(), appendStr.c_str(), ext.c_str());
#else
	int len = snprintf(fileNameTmp, fileNameNewLen, "%s%s%s%s", filePath.c_str(), fileName.c_str(), appendStr.c_str(), ext.c_str());
#endif

	if (len > 0)
	{
		fileNameTmp[len] = '\0';
		fileNameNew = fileNameTmp;
	}

	delete[] fileNameTmp;
}

char* Utils::PrintFormatBuffer(char* buffer, const size_t& bufferSize, const char* format, ...)
{
	int len = 0;

	va_list args;
	va_start(args, format);

#ifdef _WIN32
	len = vsnprintf_s(buffer, bufferSize, bufferSize - 1, format, args);
#else
	len = vsnprintf(buffer, MSG_BUFFER_MAX_LEN, format, args);
#endif

	if (len >= 0)
		buffer[len] = '\0';

	va_end(args);

	return buffer;
}

bool Utils::HexToUint(const std::string& hexValue, uint32& result)
{
    std::stringstream ss;
    ss << std::hex << hexValue;
    ss >> result;

    return !ss.fail();
}

bool Utils::ByteArrayToStringHex(const byte* data, const size_t& dataSize, std::string& out)
{
	out.clear();
	out.reserve(dataSize * 2);

	char* ptrOut = (char*)out.data();
	uint val;

	for (const byte* ptr = data; ptr != data + dataSize; ptr++)
	{
		val = *ptr;
		if (sprintf(ptrOut, "%02X", val) != 2)
			return false;

		ptrOut += 2;
	}

	return true;
}

bool Utils::LoadTextFile(const char* filename, std::string& content)
{
	std::ifstream file(filename, std::ios::in);

	if (file.is_open())
	{
		content.append((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
		file.close();
		return true;
	}

	return false;
}