#pragma once

#define SOCKET_UNDEF    -1

#include "Radford.h"

typedef enum NetProtocols
{
	NET_IPV4	= 0x1,
	NET_IPV6	= 0x2,
	NET_ALL		= NET_IPV4 | NET_IPV6
} NetProtocols;

class SocketConnector
{

public:

	SocketConnector() {}
	virtual ~SocketConnector() {}

	static SocketConnector* Create();
	static void Destroy(SocketConnector* pConn);

	virtual int Connect(const std::string& sAddress, const uint16& uPort) = 0;
	virtual int Bind(const uint16& iPort) = 0;
	virtual int Accept(const int& fd) = 0;
	virtual void Listen(const int& fd) = 0;
	virtual void Close(int& fd) = 0;
	virtual void Shutdown(int& fd) = 0;

};
