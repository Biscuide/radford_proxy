#ifdef _WIN32

#include "socket/WinSocketConnector.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <windows.h>

void WinSocketConnector::Initialize()
{
	LOG_TRACE("WinSocketConnector::Initialize");

	WSADATA wsaData;

	INT res = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (res != 0)
	{
		LOG_ERRORF("WinSocketConnector::Initialize - WSAStartup failed [result: %d]", res);
		CException::Throw(CException::WinSockError, "WSAStartup failed.");
	}
}

void WinSocketConnector::Deinitialize()
{
	LOG_TRACE("WinSocketConnector::Deinitialize");

	INT res = WSACleanup();

	if (res != 0)
	{
		LOG_ERRORF("WinSocketConnector::Deinitialize - WSACleanup failed [result: %d]", res);
		CException::Throw(CException::WinSockError, "Cannot close socket.");
	}
}

int WinSocketConnector::Connect(const std::string& sAddress, const uint16& uPort)
{
	LOG_TRACE("WinSocketConnector::Connect");

	SOCKET sock = INVALID_SOCKET;

	struct hostent* host = gethostbyname(sAddress.c_str());

	if (host == NULL)
		CException::Throw(CException::SocketError, "Cannot get server hostname.");

	if (host->h_addrtype != AF_INET)
		CException::Throw(CException::SocketError, "Not supported network type.");

	LOG_DEBUGF("WinSocketConnector::Connect - Official hostname: %s", host->h_name);

	struct in_addr addr;
	addr.s_addr = *(u_long *)host->h_addr_list[0];

	LOG_DEBUGF("WinSocketConnector::Connect - IP Address: %s", inet_ntoa(addr));

	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (sock == INVALID_SOCKET)
	{
		LOG_ERRORF("WinSocketConnector::Connect - Error while creating socket. Error: %u", WSAGetLastError());
		CException::Throw(CException::SocketError, "WinSocketConnector::Connect - Error while creating socket");
	}

	SOCKADDR_IN sockAddr;
	memset(&sockAddr, 0, sizeof(SOCKADDR_IN));
	sockAddr.sin_port = htons(uPort);
	if (CONFIG->GetIntValue(NET_PROTOCOL_VERSION) == NET_ALL)
		sockAddr.sin_family = AF_UNSPEC;
	else if ((CONFIG->GetIntValue(NET_PROTOCOL_VERSION) & NET_IPV6) > 0)
		sockAddr.sin_family = AF_INET6;
	else
		sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

	LOG_DEBUG("WinSocketConnector::Connect - Connecting...");

	if (connect(sock, (SOCKADDR*)(&sockAddr), sizeof(sockAddr)) == SOCKET_ERROR)
	{
		LOG_ERRORF("WinSocketConnector::Connect - Error while connect to server. Error: %u", WSAGetLastError());
		CException::Throw(CException::SocketError, "Cannot connect to the server.");
	}

	LOG_DEBUGF("WinSocketConnector::Connect - Connection successful (socket: %i).", sock);

	return sock;
}

int WinSocketConnector::Bind(const uint16& iPort)
{
	LOG_TRACE("WinSocketConnector::Bind");

	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == INVALID_SOCKET)
	{
		LOG_ERRORF("WinSocketConnector::Bind - Error while creating socket. Error: %u", WSAGetLastError());
		CException::Throw(CException::SocketError, "WinSocketConnector::Bind - Error while creating socket");
	}

	sockaddr_in sockAddr;
	int sockAddrSize = sizeof(sockAddr);
	memset(&sockAddr, 0, sockAddrSize);

	sockAddr.sin_port = htons(iPort);
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(sock, (struct sockaddr*)&sockAddr, sockAddrSize) == SOCKET_ERROR)
	{
		LOG_ERRORFT((int)sock, "WinSocketConnector::Bind - Error while binding socket (port: %u). Error: %u", iPort, WSAGetLastError());
		int sockTmp = sock;
		Close(sockTmp);
		CException::Throw(CException::SocketError, "UnixSocketConnector::Bind - Error while binding socket");
		return 1;
	}

	return sock;
}

int WinSocketConnector::Accept(const int& fd)
{
	LOG_TRACE("WinSocketConnector::Accept");

	struct sockaddr_in clientAddr;
	int clientAddrSize = sizeof(clientAddr);

	SOCKET sock = accept(fd, (struct sockaddr*)&clientAddr, &clientAddrSize);
	if (sock == INVALID_SOCKET)
	{
		LOG_ERRORF("WinSocketConnector::Accept - Error while accept incomming client. Error: %u", WSAGetLastError());
		return SOCKET_UNDEF;
	}

	return (int)sock;
}

void WinSocketConnector::Listen(const int& fd)
{
	LOG_TRACE("WinSocketConnector::Listen");

	if (listen((SOCKET)fd, 10) == SOCKET_ERROR)
	{
		LOG_ERRORF("WinSocketConnector::Listen - Error while start listening. Error: %u", WSAGetLastError());
		CException::Throw(CException::SocketError, "Error while start listening.");
	}
}

void WinSocketConnector::Close(int& fd)
{
	LOG_TRACE("WinSocketConnector::Close");

	if (fd != INVALID_SOCKET)
	{
		INT res = closesocket((SOCKET)fd);
		fd = INVALID_SOCKET;

		if (res != 0)
		{
			LOG_ERRORF("WinSocketConnector::Close - closesocket failed [result: %d]", res);
			//CException::Throw(CException::SocketError, "Cannot close socket.");
		}
	}
}

void WinSocketConnector::Shutdown(int& fd)
{
	LOG_TRACE("WinSocketConnector::Shutdown");

	if (fd != INVALID_SOCKET)
	{
		INT res = shutdown((SOCKET)fd, SD_BOTH);
		fd = INVALID_SOCKET;

		if (res != 0)
		{
			LOG_ERRORF("WinSocketConnector::Shutdown - shutdown failed [result: %d]. Error: %i", res, WSAGetLastError());
			//CException::Throw(CException::SocketError, "Cannot close socket.");
		}
	}
}

#endif
