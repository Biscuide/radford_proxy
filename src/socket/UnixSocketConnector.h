#pragma once

#ifdef UNIX

#include "socket/SocketConnector.h"

class UnixSocketConnector : public SocketConnector
{

public:

	int Connect(const std::string& sAddress, const uint16& iPort);
	int Bind(const uint16& iPort);
	int Accept(const int& fd);
	void Listen(const int& fd);
	void Close(int& fd);
	void Shutdown(int& fd);

};

#endif