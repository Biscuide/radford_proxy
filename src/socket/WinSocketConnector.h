#pragma once

#ifdef _WIN32

#include "socket/SocketConnector.h"

class WinSocketConnector : public SocketConnector
{

public:

	static void Initialize();
	static void Deinitialize();

	int Connect(const std::string& sAddress, const uint16& uPort);
	int Bind(const uint16& iPort);
	int Accept(const int& fd);
	void Listen(const int& fd);
	void Close(int& fd);
	void Shutdown(int& fd);

};

#endif