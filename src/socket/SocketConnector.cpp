#include "socket/SocketConnector.h"
#include "socket/WinSocketConnector.h"
#include "socket/UnixSocketConnector.h"
#include "framework/Utils.h"

SocketConnector* SocketConnector::Create()
{
#ifdef _WIN32
	return new WinSocketConnector();
#else
	return new UnixSocketConnector();
#endif
}

void SocketConnector::Destroy(SocketConnector* pConn)
{
	if (pConn)
		delete pConn;
}
