﻿#ifdef UNIX

#include "socket/UnixSocketConnector.h"
#include "framework/Utils.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

int UnixSocketConnector::Connect(const std::string& sAddress, const uint16& uPort)
{
	LOG_TRACE("UnixSocketConnector::Connect");

	LOG_DEBUGF("UnixSocketConnector::Connect - [address: %s]", sAddress.c_str());

	int iSocketFD = SOCKET_UNDEF;

	struct addrinfo ahints;
	struct addrinfo* paRes;

	memset(&ahints, 0, sizeof(ahints));
	if (CONFIG->GetIntValue(NET_PROTOCOL_VERSION) == NET_ALL)
		ahints.ai_family = AF_UNSPEC;
	else if ((CONFIG->GetIntValue(NET_PROTOCOL_VERSION) & NET_IPV6) > 0)
		ahints.ai_family = AF_INET6;
	else
		ahints.ai_family = AF_INET;
	ahints.ai_socktype = SOCK_STREAM;

	std::string sPort;
	if (Utils::lexical_cast<std::string, uint16>(uPort, sPort) == false)
		CException::Throw(CException::InternalError, "Cannot convert port from int to string data type");

	LOG_DEBUGF("UnixSocketConnector::Connect - Get address info (%s:%s).", sAddress.c_str(), sPort.c_str());

	if (getaddrinfo(sAddress.c_str(), sPort.c_str(), &ahints, &paRes) != 0)
	{
		LOG_ERRORF("UnixSocketConnector::Connect - Cannot create adress info from %s", sAddress.c_str());
		CException::Throw(CException::NetworkError, "Cannot create adress info");
	}

	LOG_DEBUG("UnixSocketConnector::Connect - Creating socket ...");

	if ((iSocketFD = socket(paRes->ai_family, paRes->ai_socktype, paRes->ai_protocol)) < 0)
	{
		LOG_ERROR("UnixSocketConnector::Connect - Cannot create socket");
		CException::Throw(CException::NetworkError, "Cannot create socket");
	}

	LOG_DEBUG("UnixSocketConnector::Connect - Connecting ...");

	if (connect(iSocketFD, paRes->ai_addr, paRes->ai_addrlen) < 0)
	{
		LOG_ERRORF("UnixSocketConnector::Connect - Cannot connect to %s (socket %i)", sAddress.c_str(), iSocketFD);
		CException::Throw(CException::NetworkError, "Cannot connect to the server");
	}

	LOG_DEBUGF("UnixSocketConnector::Connect - Socket FD: %i", iSocketFD);

	freeaddrinfo(paRes);

	return iSocketFD;
}

int UnixSocketConnector::Bind(const uint16& iPort)
{
	LOG_TRACE("UnixSocketConnector::Bind");

	int iSocketFD = socket(AF_INET, SOCK_STREAM, 0);

	if (iSocketFD == SOCKET_UNDEF)
	{
		LOG_ERROR("UnixSocketConnector::Bind - Error while creating socket");
		CException::Throw(CException::SocketError, "UnixSocketConnector::Bind - Error while creating socket");
	}

	LOG_DEBUGF("UnixSocketConnector::Bind - Socket: %i", iSocketFD);

	int enable = 1;
	if (setsockopt(iSocketFD, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
	{
		LOG_ERRORT(iSocketFD, "UnixSocketConnector::Bind - Error while setting socket option");
		Close(iSocketFD);
		CException::Throw(CException::SocketError, "UnixSocketConnector::Bind - Error while setting socket option");
	}

	struct sockaddr_in proxyAddr;
	memset(&proxyAddr, 0, sizeof(struct sockaddr_in));
	proxyAddr.sin_family = AF_INET;
	proxyAddr.sin_addr.s_addr = INADDR_ANY;
	proxyAddr.sin_port = htons(iPort);

	if (bind(iSocketFD, (struct sockaddr*) &proxyAddr, sizeof(proxyAddr)) < 0)
	{
		LOG_ERRORFT(iSocketFD, "UnixSocketConnector::Bind - Error while binding socket (port: %u)", iPort);
		Close(iSocketFD);
		CException::Throw(CException::SocketError, "UnixSocketConnector::Bind - Error while binding socket");
	}

	return iSocketFD;
}

int UnixSocketConnector::Accept(const int& fd)
{
	LOG_TRACE("UnixSocketConnector::Accept");

	struct sockaddr_in clientAddr;
	uint clientAddrSize = sizeof(clientAddr);

	return accept(fd, (struct sockaddr*)&clientAddr, &clientAddrSize);
}

void UnixSocketConnector::Listen(const int& fd)
{
	LOG_TRACE("UnixSocketConnector::Listen");

	if (listen(fd, 10) < 0)
	{
		LOG_ERROR("UnixSocketConnector::Listen - Error while start listening");
		CException::Throw(CException::SocketError, "UnixSocketConnector::Listen - Error while start listening");
	}
}

void UnixSocketConnector::Close(int& fd)
{
	LOG_TRACE("UnixSocketConnector::Close");

	if (fd != SOCKET_UNDEF && close(fd) != 0)
		LOG_ERRORF("UnixSocketConnector::Close - Error in socket close: %i", fd);

	fd = SOCKET_UNDEF;
}

void UnixSocketConnector::Shutdown(int& fd)
{
	LOG_TRACE("UnixSocketConnector::Shutdown");

	if (fd != SOCKET_UNDEF && shutdown(fd, SHUT_RDWR) != 0)
		LOG_ERRORF("UnixSocketConnector::Shutdown - Error in socket shutdown: %i", fd);

	fd = SOCKET_UNDEF;
}

#endif
