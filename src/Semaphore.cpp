#include "Semaphore.h"

using namespace std;

Semaphore::Semaphore(const uint32& val) : _val(val)
{

}

void Semaphore::Notify()
{
	unique_lock<mutex> lck(_mutex);

	_val++;

	_conVar.notify_one();
}

void Semaphore::NotifyAll()
{
	unique_lock<mutex> lck(_mutex);

	_val++;

	_conVar.notify_one();
}

void Semaphore::Wait(bool* shutdown)
{
	unique_lock<mutex> lck(_mutex);

	while (_val == 0 && !*shutdown)
	{
		_conVar.wait(lck);
	}

	_val--;
}
