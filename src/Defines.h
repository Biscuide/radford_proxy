#pragma once

#include <stdint.h>
#include <vector>
#include <string>
#include <map>
#include <queue>
#include <utility>

typedef unsigned char byte;
typedef unsigned int uint;

typedef int8_t int8;
typedef int32_t int32;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef int16_t int16;
typedef uint32_t uint32;
typedef int32_t int32;
typedef uint64_t uint64;
typedef int64_t int64;

typedef std::vector<std::string> StringVector;
typedef std::vector<byte> ByteVector;

#define MSG_BUFFER_MAX_LEN 4096
