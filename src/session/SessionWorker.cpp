#include "session/SessionWorker.h"
#include "session/Session.h"

SessionWorker::SessionWorker(ProducerConsumerQueue<Session*>* pQueue)
{
	LOG_TRACE("SessionWorker::SessionWorker");

    _queue = pQueue;
	_shutdown = false;
	_thread = std::thread(&SessionWorker::Start, this);
}

SessionWorker::~SessionWorker()
{
	LOG_TRACE("SessionWorker::~SessionWorker");

	_shutdown = true;

	_thread.join();
}

void SessionWorker::Start()
{
	LOG_TRACE("SessionWorker::Start");

	if (!_queue)
		return;

	while (true)
	{
		Session* t = NULL;

		_queue->WaitPop(t);

		if (_shutdown || !t)
			break;

        t->StartProxyConnection();

		delete t;
	}
}
