#include "session/SessionWorkerPool.h"
#include "session/Session.h"
#include "session/SessionWorker.h"

SessionWorkerPool::SessionWorkerPool()
{
	LOG_TRACE("SessionWorkerPool::SessionWorkerPool");
    for (uint8 i = 0; i < PROTOCOL_MAX; i++)
    {
        _queue[i] = std::unique_ptr<ProducerConsumerQueue<Session*>>(new ProducerConsumerQueue<Session*>(CONFIG->GetIntValue(PROXY_PROTOCOL_THREADS)));
    }
}

SessionWorkerPool::~SessionWorkerPool()
{
	LOG_TRACE("SessionWorkerPool::~SessionWorkerPool");
}

void SessionWorkerPool::StartWorkers()
{
	LOG_TRACE("SessionWorkerPool::StartWorkers");

    for (uint8 i = 0; i < PROTOCOL_MAX; i++)
    {
        for (uint16 j = 0; j < CONFIG->GetIntValue(PROXY_PROTOCOL_THREADS); j++)
        {
            _workers[i].push_back(std::unique_ptr<SessionWorker>(new SessionWorker(_queue[i].get())));
        }
    }
}

void SessionWorkerPool::StopWorkers()
{
	LOG_TRACE("SessionWorkerPool::StopWorkers");

    for (uint8 i = 0; i < PROTOCOL_MAX; i++)
    {
        _queue[i]->Cancel();
    }

    for (uint8 i = 0; i < PROTOCOL_MAX; i++)
    {
        _workers[i].clear();
    }
}

void SessionWorkerPool::EnqueueSession(Session* s)
{
	LOG_TRACE("SessionWorkerPool::EnqueueSession");

    _queue[s->GetInfo()->protocolName]->Push(s);
}
