#pragma once

#include "http/HttpClient.h"
#include "ProducerConsumerQueue.h"

class Session;
class SessionWorker;

class SessionWorkerPool
{

public:

    SessionWorkerPool();
    ~SessionWorkerPool();

    void StartWorkers();
    void StopWorkers();

    void EnqueueSession(Session* s);

private:

    std::unique_ptr<ProducerConsumerQueue<Session*>> _queue[PROTOCOL_MAX];
    std::vector<std::unique_ptr<SessionWorker>> _workers[PROTOCOL_MAX];

};
