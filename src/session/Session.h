#pragma once

#include "http/HttpClient.h"

#include <openssl/ssl.h>

typedef enum ConnectionType
{
	CONNECTION_CLIENT,
	CONNECTION_SERVER,
	CONNECTION_MAX
} ConnectionType;

typedef struct SessionInfo
{
	SessionInfo() {}
	SessionInfo(ProtocolName protocol, IntConfigItems configInputPort, IntConfigItems configOutputPort, BoolConfigItems configEnable)
		: protocolName(protocol), configEnable(configEnable)
	{
		configPort[CONNECTION_CLIENT] = configInputPort;
		configPort[CONNECTION_SERVER] = configOutputPort;
	}

	ProtocolName protocolName;
	IntConfigItems configPort[CONNECTION_MAX];
	BoolConfigItems	configEnable;
} SessionInfo;

typedef enum SessionType
{
	SESSION_UNK,
	SESSION_PROXY,
	SESSION_TUNNEL
} SessionType;

class Session
{

public:

    Session(const int& iClientSocketFD, const SessionInfo* pInfo);
	Session(const int& iClientSocketFD, SSL* pClientSSL, const SessionInfo* pInfo);
    virtual ~Session();

    void StartProxyConnection();
	void SwitchToTunneling();

	const SessionInfo* GetInfo() const { return _pInfo; }
	const SessionType GetType() const { return _type; }

protected:

    void Close();
	virtual HttpClient* CreateHttpClient(ConnectionType type) const;

	SessionType _type;
	HttpClient* _pConn[CONNECTION_MAX];
    int _iClientSocketFD;
	SSL* _pClientSSL;
	const SessionInfo* _pInfo;
};
