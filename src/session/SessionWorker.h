#pragma once

#include "Radford.h"
#include "ProducerConsumerQueue.h"

#include <thread>

class SessionWorker
{

public:

    SessionWorker(ProducerConsumerQueue<Session*>* pQueue);
    virtual ~SessionWorker();

private:

    void Start();

private:

    bool _shutdown;
	ProducerConsumerQueue<Session*>* _queue;
	std::thread _thread;
};
