#include "session/Session.h"
#include "http/HttpRequest.h"
#include "http/HttpsClient.h"

#include <thread>

#ifdef _WIN32
#include <string.h>
#else
#include <strings.h>
#include <unistd.h>
#endif

Session::Session(const int& iClientSocketFD, const SessionInfo* pInfo)
	: _type(SESSION_UNK), _iClientSocketFD(iClientSocketFD), _pClientSSL(NULL), _pInfo(pInfo)
{
	LOG_TRACET(_iClientSocketFD, "Session::Session");

	for (uint8 i = 0; i < CONNECTION_MAX; i++)
	{
		_pConn[i] = NULL;
	}
}

Session::Session(const int& iClientSocketFD, SSL* pClientSSL, const SessionInfo* pInfo)
	: _type(SESSION_UNK), _iClientSocketFD(iClientSocketFD), _pClientSSL(pClientSSL), _pInfo(pInfo)
{
	LOG_TRACET(_iClientSocketFD, "Session::Session");

	for (uint8 i = 0; i < CONNECTION_MAX; i++)
	{
		_pConn[i] = NULL;
	}
}

Session::~Session()
{
	LOG_TRACET(_iClientSocketFD, "Session::~Session");

	for (uint8 i = 0; i < CONNECTION_MAX; i++)
	{
		if (_pConn[i])
		{
			if (!_pConn[i]->IsClosed())
				_pConn[i]->Close();

			delete _pConn[i];
		}
	}
}

void Session::StartProxyConnection()
{
	LOG_TRACET(_iClientSocketFD, "Session::StartProxyConnection");

    if (_iClientSocketFD == SOCKET_UNDEF)
    {
		LOG_WARNINGT(_iClientSocketFD, "Session::StartProxyConnection - Undefined client socket FD");
		Close();
        return;
    }

	uint16 uPort = CONFIG->GetIntValue(_pInfo->configPort[CONNECTION_SERVER]);
	LOG_DEBUGFT(_iClientSocketFD, "Session::StartProxyConnection - Protocol: %s(%u), port: %u", GetProtocolName(_pInfo->protocolName).c_str(), _pInfo->protocolName, uPort);
	_type = SESSION_PROXY;

	try
	{
		_pConn[CONNECTION_CLIENT] = CreateHttpClient(CONNECTION_CLIENT);
		_pConn[CONNECTION_SERVER] = CreateHttpClient(CONNECTION_SERVER);

		uint16 connStatus[CONNECTION_MAX];
		for (uint8 i = 0; i < CONNECTION_MAX; i++)
		{
			connStatus[i] = HttpClient::RS_UNK;
		}

		HttpResponse proxyResponse;
		bool tunnelSwitch = false;

		while (true)
		{
			try
			{
				if (tunnelSwitch == true)
				{
					SwitchToTunneling();
					break;
				}
				else
				{
					connStatus[CONNECTION_CLIENT] = _pConn[CONNECTION_CLIENT]->ResendRequest(_pConn[CONNECTION_SERVER], uPort, &proxyResponse);
					if ((connStatus[CONNECTION_CLIENT] & ((uint16)HttpClient::RS_CLOSED | (uint16)HttpClient::RS_ERROR)) > 0)
						break;

					if ((connStatus[CONNECTION_CLIENT] & (uint16)HttpClient::RS_PROXY_RESP) > 0)
					{
						_pConn[CONNECTION_CLIENT]->SendResponseHeader(&proxyResponse);
						proxyResponse.Reset();

						if ((connStatus[CONNECTION_CLIENT] & (uint16)HttpClient::RS_TUNNELING) > 0)
						{
							tunnelSwitch = true;
							continue;
						}
					}
					else
					{
						connStatus[CONNECTION_SERVER] = _pConn[CONNECTION_SERVER]->ResendResponse(_pConn[CONNECTION_CLIENT]);

						if ((connStatus[CONNECTION_SERVER] & ((uint16)HttpClient::RS_CLOSED | (uint16)HttpClient::RS_CLOSE | (uint16)HttpClient::RS_ERROR)) > 0)
							break;
					}

					if ((connStatus[CONNECTION_CLIENT] & (uint16)HttpClient::RS_CLOSE) > 0)
						break;
				}
			}
			catch (const CException& ex)
			{
				PrintEx(ex);
				break;
			}
		}
	}
	catch (const CException& ex)
	{
		PrintEx(ex);
	}

    Close();
}

void Session::SwitchToTunneling()
{
	LOG_TRACET(_iClientSocketFD, "Session::SwitchToTunneling");

	_type = SESSION_TUNNEL;

	std::thread threadClient(&HttpClient::ResendData, _pConn[CONNECTION_CLIENT], _pConn[CONNECTION_SERVER]);
	std::thread threadServer(&HttpClient::ResendData, _pConn[CONNECTION_SERVER], _pConn[CONNECTION_CLIENT]);

	threadServer.join();
	threadClient.join();

	Close();
}

void Session::Close()
{
	LOG_TRACET(_iClientSocketFD, "Session::Close");

	for (uint8 i = 0; i < CONNECTION_MAX; i++)
	{
		if (_pConn[i])
			_pConn[i]->Close();
	}
}

HttpClient* Session::CreateHttpClient(ConnectionType type) const
{
	LOG_TRACET(_iClientSocketFD, "Session::CreateHttpClient");

	if (type == CONNECTION_CLIENT)
	{
		if (_pInfo->protocolName == PROTOCOL_HTTP)
			return new HttpClient(&_iClientSocketFD);

		return new HttpsClient(_pClientSSL, &_iClientSocketFD);
	}
	else if (type == CONNECTION_SERVER)
	{
		if (_pInfo->protocolName == PROTOCOL_HTTP)
			return new HttpClient();

		return new HttpsClient();
	}

	return NULL;
}
