#pragma once

#include "Defines.h"

#include "framework/CException.h"
#include "framework/Config.h"
#include "framework/Logger.h"

void PrintEx(const CException& ex);
