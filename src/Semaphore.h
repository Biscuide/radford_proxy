#pragma once

#include "Radford.h"

#include <mutex>
#include <condition_variable>

class Semaphore
{

public:

	Semaphore(const uint32& val);

	void Notify();
	void NotifyAll();
	void Wait(bool* shutdown);

private:

  std::mutex _mutex;
  std::condition_variable _conVar;
  uint32 _val;
};
