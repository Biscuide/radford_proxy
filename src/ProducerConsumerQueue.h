#pragma once

#include "Radford.h"
#include "Semaphore.h"

#define Median(a,b,c) ((a > b) ? std::max(b, std::min(a,c)) : std::min(b, std::max(a, c)))

template <typename T>
class ProducerConsumerQueue
{
public:

	ProducerConsumerQueue<T>(const uint8& threadCount) : _semFree(Median(threadCount * 2, 10, 50)), _semFull(0)
	{
		LOG_TRACE("ProducerConsumerQueue::ProducerConsumerQueue");

		_shutdown = false;
	}

	void Push(const T& val)
	{
		_semFree.Wait(&_shutdown);

		_mutex.lock();

		_queue.push(val);

		_mutex.unlock();

		_semFull.Notify();
	}

	bool Pop(T& val)
	{
		_semFull.Wait(&_shutdown);

		_mutex.lock();

		if (_queue.empty() || _shutdown)
		{
			_mutex.unlock();
			_semFull.Notify();
			return false;
		}

		val = _queue.front();

		_queue.pop();

		_mutex.unlock();

		_semFree.Notify();

		return true;
	}

	bool WaitPop(T& val)
	{
		_semFull.Wait(&_shutdown);

		_mutex.lock();

		if (_queue.empty() || _shutdown)
		{
			_mutex.unlock();
			_semFull.Notify();
			return false;
		}

		val = _queue.front();

		_queue.pop();

		_mutex.unlock();

		_semFree.Notify();

		return true;
	}

	void Cancel()
	{
		LOG_TRACE("ProducerConsumerQueue::Cancel");

		_mutex.lock();

		while (!_queue.empty())
		{
            delete _queue.front();
			_queue.pop();
		}

		_shutdown = true;

		_semFree.NotifyAll();
		_semFull.NotifyAll();

		_mutex.unlock();
	}

private:

	bool _shutdown;
	std::mutex _mutex;
	Semaphore _semFree;
	Semaphore _semFull;
	std::queue<T> _queue;
};

