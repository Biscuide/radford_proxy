#include "proxy/ProxyMgr.h"
#include "DataMgr.h"

#include "framework/Logger.h"
#ifdef _WIN32
#include "socket/WinSocketConnector.h"
#endif

#include <iostream>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/conf.h>

void PrintEx(const CException& ex)
{
	LOG_ERRORF("ID: %u", ex.GetId());
	LOG_ERRORF("Msg: %s", ex.GetMsg().c_str());
	LOG_ERRORF("Description: %s", ex.GetDescription().c_str());
}

void InitializeOpenSSL()
{
	SSL_load_error_strings();
	OpenSSL_add_all_digests();
	OpenSSL_add_all_ciphers();
	if (OpenSSL_add_ssl_algorithms() != 1)
		CException::Throw(CException::OpenSSLError, "Error while initializing OpenSSL (OpenSSL_add_ssl_algorithms)");
}

void sslCompFree(SSL_COMP *cm)
{
	OPENSSL_free(cm);
}

void DeinitializeOpenSSL()
{
	FIPS_mode_set(0);
	CONF_modules_unload(1);
	OBJ_cleanup();
	EVP_cleanup();
	if (SSL_COMP_get_compression_methods())
		sk_SSL_COMP_pop_free(SSL_COMP_get_compression_methods(), sslCompFree);
	CRYPTO_cleanup_all_ex_data();
	ERR_remove_state(0);
	ERR_free_strings();
}

bool Initialize()
{
	try
	{
		fprintf(stdout, "Config initialize ...\n");
		Config::Initialize();

		fprintf(stdout, "Config loading ...\n");
		CONFIG->Load();

		fprintf(stdout, "Logger initialize ...\n");
		Logger::Initialize();

#ifdef _WIN32
		fprintf(stdout, "Windows sockets initialize ...\n");
		WinSocketConnector::Initialize();
#endif
		fprintf(stdout, "OpenSSL initialize ...\n");
		InitializeOpenSSL();

		fprintf(stdout, "DataMgr initialize ...\n");
		DataMgr::Initialize();

		fprintf(stdout, "ProxyMgr initialize ...\n");
		ProxyMgr::Initialize();
	}
	catch (const CException& ex)
	{
		if (Logger::IsInitialized())
		{
			LOG_ERROR("Nastala chyba v inicializaci programu!")
			PrintEx(ex);
		}
		else
		{
			fprintf(stderr, "Nastala chyba v inicializaci programu!\n");
			fprintf(stderr, "ID: %u\n", ex.GetId());
			fprintf(stderr, "Msg: %s\n", ex.GetMsg().c_str());
			fprintf(stderr, "Description: %s\n", ex.GetDescription().c_str());
		}

		return false;
	}

	return true;
}

void Deinitialize()
{
	try
	{
		fprintf(stdout, "DataMgr deinitialize ...\n");
		DataMgr::Deinitialize();

		fprintf(stdout, "ProxyMgr deinitialize ...\n");
		ProxyMgr::Deinitialize();

#ifdef _WIN32
		fprintf(stdout, "Windows sockets deinitialize ...\n");
		WinSocketConnector::Deinitialize();
#endif
		fprintf(stdout, "OpenSSL deinitialize ...\n");
		DeinitializeOpenSSL();

		fprintf(stdout, "Logger deinitialize ...\n");
		Logger::Deinitialize();

		fprintf(stdout, "Config deinitialize ...\n");
		Config::Deinitialize();
	}
	catch (const CException& ex)
	{
		LOG_ERROR("Nastala chyba v ukoncovani programu!")
		PrintEx(ex);
	}
}

int main(int argc, char** argv)
{
    fprintf(stdout, "Radford - HTTP(S) proxy and tunneling\n");
	fprintf(stdout, "Created by: %s\n", "Smid Tomas");
	fprintf(stdout, "Initialize ...\n");

    if (Initialize() == false)
		return 1;

    try
    {
        PROXYMGR->Start();

		// Reading from console
		std::string sInLine;

		while (!PROXYMGR->IsClosing())
		{
			std::getline(std::cin, sInLine);

			//LOG_INFOF("Input line: \"%s\"", sInLine.c_str());

			if (sInLine == "quit")
				PROXYMGR->Close();

			std::this_thread::sleep_for(std::chrono::microseconds(100));
		}
    }
    catch (const CException& ex)
    {
		PrintEx(ex);
    }

	fprintf(stdout, "Deinitilize ...\n");

	Deinitialize();

	return 0;
}
